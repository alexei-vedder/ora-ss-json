export namespace ExtraInput {
	export interface ResponseBody {
		message: string;
		options: any[];
		examples: any[];
		mode: SelectMode
	}

	export interface RequestBody {
		answer: any[];
	}

	export enum SelectMode {
		Free,
		One,
		AtLeastOne
	}
}

export namespace SchemaSpawned {
	export interface ResponseBody {
		schemaId: string;
		dataId: string;
	}
}

export namespace Notification {
	export interface ResponseBody {
		type: Type;
		message: string;
		action?: string;
	}

	export enum Type {
		Error,
		Info,
	}
}

export namespace ProcessingProgress {
	export interface ResponseBody {
		task: string;
		/** in range of [0, 100] */
		value?: number;
	}
}

export enum WebSocketEvent {
	DataUpload = "data-upload",
	ExtraInput = "extra-input",
	ProcessingProgress = "processing-progress",
	SchemaSpawned = "schema-spawned",
	GetSchema = "get-schema",
	GetSchemasIds = "get-schemas-ids",
	GetDataInstance = "get-data-instance",
	GetDataInstancesIds = "get-data-instances-ids",
	Notification = "notification",
	DeleteDataInstance = "delete-data-instance",
	DeleteSchema = "delete-schema",
}
