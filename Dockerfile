FROM node:14

RUN mkdir -p /home/node/app/core-app/node_modules && chown -R node:node /home/node/app

RUN mkdir -p /home/node/app/shared-models

WORKDIR /home/node/app

COPY core-app/package*.json ./core-app/

USER node

WORKDIR /home/node/app/core-app

RUN npm i

WORKDIR /home/node/app

COPY --chown=node:node core-app ./core-app/

COPY ./shared-models ./shared-models/

WORKDIR /home/node/app/core-app

RUN npm run build

EXPOSE 8080

CMD [ "node", "dist/core-app/src/app.js" ]
