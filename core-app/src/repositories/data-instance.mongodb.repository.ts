import { Injectable } from "@nestjs/common";
import { DataInstance, DataInstanceDocument } from "../models/repository-entity.model";
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";
import { AbstractChunkedMongoDBRepository } from "./abstract-chunked.mongodb.repository";
import { ChunkMongoDBRepository } from "./chunk.mongodb.repository";
import { ParsingService } from "../services/parsing.service";

@Injectable()
export class DataInstanceMongoDBRepository extends AbstractChunkedMongoDBRepository<DataInstance> {

	constructor(@InjectModel(DataInstance.name)
				protected dataInstanceModel: Model<DataInstanceDocument>,
				protected chunkRepository: ChunkMongoDBRepository,
				protected parser: ParsingService) {
		super(dataInstanceModel, chunkRepository, parser);
	}
}
