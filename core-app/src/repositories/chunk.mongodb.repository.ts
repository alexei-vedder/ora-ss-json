import { Injectable } from "@nestjs/common";
import { Repository } from "./repository";
import { InjectModel } from "@nestjs/mongoose";
import { Chunk, ChunkDocument } from "../models/repository-entity.model";
import { Model } from "mongoose";

@Injectable()
export class ChunkMongoDBRepository implements Repository<Chunk> {

	constructor(@InjectModel(Chunk.name)
				private readonly chunkModel: Model<ChunkDocument>) {
	}

	async put(data: Chunk): Promise<void> {
		const newChunk = new this.chunkModel(data);
		await newChunk.save();
	}

	async remove(id: string): Promise<void> {
		// @ts-ignore
		return this.chunkModel.deleteOne({ "id": id }).exec();
	}

	async retrieve(id: string): Promise<Chunk> {
		// @ts-ignore
		return this.chunkModel.findOne({ "id": id }).exec();
	}

	async retrieveAll(): Promise<any[]> {
		return this.chunkModel.find().exec();
	}
}
