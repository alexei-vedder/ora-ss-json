import { Repository } from "./repository";
import { Chunk, ChunkedRepositoryEntity } from "../models/repository-entity.model";
import { Document, Model } from "mongoose";
import { ChunkMongoDBRepository } from "./chunk.mongodb.repository";
import { ParsingService } from "../services/parsing.service";
import { v4 as uuidv4 } from "uuid";
import { finalize } from "rxjs/operators";
import { Observable, Subscriber } from "rxjs";

export class AbstractChunkedMongoDBRepository<T extends ChunkedRepositoryEntity> implements Repository<T> {

	constructor(protected model: Model<T & Document>,
				protected chunkRepository: ChunkMongoDBRepository,
				protected parser: ParsingService) {
	}

	async put(data: T): Promise<void> {
		return new Promise((resolve, reject) => {
			const chunkIds: string[] = [];
			this.parser.stringifyStream(data.value)
				.pipe(finalize(async () => {
					const chunkedData = { ...data };
					/* replacing "value" with "chunkIds" */
					delete chunkedData.value;
					chunkedData.chunkIds = chunkIds;
					const entity = new this.model(chunkedData);
					await entity.save();
					resolve();
				}))
				.subscribe(async (chunk: string) => {
					const chunkId = uuidv4();
					chunkIds.push(chunkId);
					await this.chunkRepository.put({
						id: chunkId,
						value: chunk,
						entityId: data.id,
					});
				});
		});
	}

	async remove(id: string): Promise<boolean> {
		try {
			// @ts-ignore
			const entity: T = await this.model.findOneAndDelete({ "id": id }).exec();
			for (let chunkId of entity.chunkIds) {
				await this.chunkRepository.remove(chunkId);
			}
			console.log(`Entity ${id} has been deleted`);
			return true;
		} catch (e) {
			console.error(`Error during deleting the entity ${id}`);
			return false;
		}
	}

	async retrieve(id: string): Promise<T> {
		// @ts-ignore
		const entity: T = await this.model.findOne({ "id": id }).exec();
		return await this.restoreChunkedEntity(entity);
	}

	async retrieveAll(): Promise<T[]> {
		const entities: T[] = await this.model.find().exec();
		const restoredEntities: T[] = [];
		for (let entity of entities) {
			restoredEntities.push(await this.restoreChunkedEntity(entity));
		}
		return restoredEntities;
	}

	async retrieveIds(): Promise<string[]> {
		return (await this.model.find({}, "id") as any[]).map(item => item.id);
	}

	protected async restoreChunkedEntity(entity: T): Promise<T> {

		const restoredEntity = JSON.parse(JSON.stringify(entity));

		if (!entity.chunkIds?.length) {
			return restoredEntity;
		}

		const chunk$ = new Observable<string>((observer: Subscriber<string>) => {
			const asyncBuffer: Promise<Chunk>[] = entity.chunkIds.map(id => this.chunkRepository.retrieve(id));
			Promise.all(asyncBuffer).then((buffer: Chunk[]) => {
				buffer.forEach(chunk => observer.next(chunk.value));
				observer.complete();
			});
		});

		/* replacing "chunkIds" with "value" */
		restoredEntity.value = await this.parser.parseStream(chunk$);
		delete restoredEntity.chunkIds;
		return restoredEntity;
	}

}
