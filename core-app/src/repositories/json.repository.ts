import { Injectable } from "@nestjs/common";
import { readFile, writeFile } from "fs";
import { Repository } from "./repository";
import { RepositoryEntity } from "../models/repository-entity.model";
import { inspect } from "util";

@Injectable()
export class JSONRepository<T extends RepositoryEntity> implements Repository<T> {

	private readonly filePath: string;
	private cache: T[] = null;

	constructor(filePath: string) {
		this.filePath = filePath;
	}

	async retrieveAll(): Promise<T[]> {
		if (this.cache) {
			return this.cache;
		}
		return new Promise((resolve => {
			readFile(this.filePath, (error, buffer) => {
				if (error) {
					throw error;
				}

				// @ts-ignore
				const data = JSON.parse(buffer);

				if (Array.isArray(data)) {
					this.cache = data;
					resolve(data);
				} else {
					throw new Error("Retrieved data is not an array instance: " + JSON.stringify(data));
				}
			});
		}));
	}

	async retrieve(id: string): Promise<T> {
		const data = await this.retrieveAll();
		return data.find(value => value.id === id);
	}

	async put(json: any): Promise<void> {
		const data = await this.retrieveAll();
		const parsed = JSON.parse(json);
		data.push(parsed);
		writeFile(this.filePath, JSON.stringify(data), error => {
			if (!error) {
				this.cache = data;
				console.log(`Object ${inspect(parsed, false, 1)} has been successfully added`);
			} else {
				throw error;
			}
		});
	}

	async remove(id: string): Promise<void> {
		let data = await this.retrieveAll();
		data = data.filter(value => value.id !== id);
		writeFile(this.filePath, JSON.stringify(data), error => {
			if (!error) {
				this.cache = data;
				console.log(`Object with id = ${id} has been successfully deleted`);
			} else {
				throw error;
			}
		});
	}
}
