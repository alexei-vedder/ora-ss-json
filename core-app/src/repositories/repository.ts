import { RepositoryEntity } from "../models/repository-entity.model";

export interface Repository<T extends RepositoryEntity> {

	retrieveAll(): Promise<T[]>;

	retrieve(id: string): Promise<T>;

	put(data: T): Promise<void>;

	remove(id: string): Promise<void | boolean>;
}
