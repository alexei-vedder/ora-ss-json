import { Injectable } from "@nestjs/common";
import { ORASchemaEntity, ORASchemaEntityDocument } from "../models/repository-entity.model";
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";
import { AbstractChunkedMongoDBRepository } from "./abstract-chunked.mongodb.repository";
import { ChunkMongoDBRepository } from "./chunk.mongodb.repository";
import { ParsingService } from "../services/parsing.service";

@Injectable()
export class ORASchemaEntityMongoDBRepository extends AbstractChunkedMongoDBRepository<ORASchemaEntity> {

	constructor(@InjectModel(ORASchemaEntity.name)
				protected oraSchemaEntityDocumentModel: Model<ORASchemaEntityDocument>,
				protected chunkRepository: ChunkMongoDBRepository,
				protected parser: ParsingService) {
		super(oraSchemaEntityDocumentModel, chunkRepository, parser);
	}
}
