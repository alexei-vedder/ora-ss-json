import { Injectable } from "@nestjs/common";
import { ORASS } from "../models/ora-ss.model";
import { ValueType } from "../models/type.enum";
import { SerializingService } from "./serializing.service";
import { getType, isMultiValued, isObject, isSingleValued } from "../fns/value-type-validators";

@Injectable()
export class NodeHandlingService {

	constructor(private serializer: SerializingService) {
	}

	/**
	 * skips root element if it's an object or array (of any nesting level)
	 */
	public identifyObjects(data: unknown): ORASS.Object[] {
		if (isSingleValued(getType(data)) || this.isEmpty(data)) {
			return [];
		}

		let objects: ORASS.Object[] = [];

		if (isObject(getType(data))) {
			objects.push({
				name: ORASS.ROOT_OBJECT_NAME,
				body: <ORASS.ObjectBody>data,
			});
			objects = objects.concat(Object.keys(data)
				.flatMap(key => this.identifyObjectsRecursively(data[key], this.pascalize(key))));
		} else if (isMultiValued(getType(data))) {
			objects = objects.concat((<Array<unknown>>data)
				.flat(Infinity)
				.flatMap(item => this.identifyObjects(item)));
		}

		return objects.filter(object => !!object.body);
	}

	public identifyObjectClasses(objects: ORASS.Object[]): ORASS.ObjectClass[] {

		let oClasses: ORASS.ObjectClass[] = this.identifyAllObjectClasses(objects);

		oClasses = this.reduceObjectClasses(oClasses);

		for (let oClass of oClasses) {
			oClass.objectClasses = this.identifyReferredObjectClasses(oClass, oClasses);
		}

		return oClasses;
	}

	public identifyAllObjectClasses(objects: ORASS.Object[]): ORASS.ObjectClass[] {
		return objects.map(object => new ORASS.ObjectClass(object.name, [object], this.classifyAttrs(object.body)));
	}

	public identifyReferredObjectClasses(source: ORASS.ObjectClass, oClasses: ORASS.ObjectClass[]): ORASS.ObjectClass[] {
		const referredOClasses: ORASS.ObjectClass[] = [];
		for (let instance of source.instances) {
			for (let key in instance.body) {
				const element = instance.body[key];
				let oClass: ORASS.ObjectClass;
				if (isObject(getType(element))) {
					oClass = this.getNode(key, oClasses);
					if (oClass) {
						referredOClasses.push(oClass);
					}
				} else if (isMultiValued(getType(element))) {
					(<Array<unknown>>element).forEach(item => {
						if (isObject(getType(item))) {
							oClass = this.getNode(key + ORASS.ARRAY_ITEM_POSTFIX, oClasses);
							if (oClass) {
								referredOClasses.push(oClass);
							}
						}
					});
				}
			}
		}
		return [...new Set(referredOClasses)];
	}

	public getIdentifierValue(oClass: ORASS.ObjectClass, instance: ORASS.Object): any[] {
		return oClass.identifier.map(idPart => instance.body[idPart.name]);
	}

	public extractObjectClassUniqueIdValues(oClass: ORASS.ObjectClass): unknown[][] {
		const ids = oClass.instances.map(instance => this.getIdentifierValue(oClass, instance));
		const uniqueIds = [...new Set(ids.map(group => JSON.stringify(group)))].map(group => JSON.parse(group));
		return uniqueIds;
	}

	public removeAttributes(schema: ORASS.Schema) {
		const attrlessSchema = this.serializer.deserialize(this.serializer.serialize(schema));

		const attributes = attrlessSchema.nodeDataArray.filter(node => node instanceof ORASS.Attribute);

		attributes.forEach(attr => {
			const attributeRelatedLinks =
				attrlessSchema.linkDataArray.filter(link => link.to === attr.key || link.from === attr.key);

			attributeRelatedLinks.forEach(link => {
				attrlessSchema.removeLinkData(link);
			});

			attrlessSchema.removeNodeData(attr);
		});

		return attrlessSchema;
	}

	/**
	 * makes null the following values: [{}, [], null, undefined]
	 */
	public nullify(data: unknown): unknown {
		switch (getType(data)) {
			case ValueType.string:
			case ValueType.number:
			case ValueType.boolean:
				return data;
			case ValueType.object: {
				if (this.isEmpty(data)) {
					return null;
				}

				let object = { ...<object>data };

				for (let key in object) {
					if (this.isEmpty(object[key])) {
						object[key] = null;
						continue;
					}
					const elem = this.nullify(object[key]);
					if (this.isEmpty(elem)) {
						object[key] = null;
					} else {
						object[key] = elem;
					}
				}

				return object;
			}
			case ValueType.array: {
				if (this.isEmpty(data)) {
					return null;
				}

				let array = [].concat(...data as Array<unknown>);

				for (let i = 0; i < array.length; ++i) {
					if (this.isEmpty(array[i])) {
						(<unknown[]>array).splice(i--, 1);
						continue;
					}

					const elem = this.nullify(array[i]);
					if (this.isEmpty(elem)) {
						(<unknown[]>array).splice(i--, 1);
					} else {
						array[i] = elem;
					}
				}
				return array;
			}
			case ValueType.null: {
				return null;
			}
		}
	}

	public isEmpty(object: any): boolean {
		return (object === undefined || object === null)
			|| (Object.keys(object).length === 0 && object.constructor === Object)
			|| (Array.isArray(object) && object.length === 0);
	}

	/**
	 * Converts: kebab-case, Common Case, UPPER CASE, lower case to PascalCase
	 * WARNING! This will turn 'HelloWorld' and 'helloWorld' into 'Helloworld'
	 * Allows numbers at the beginning
	 */
	public pascalize(str: string): string {
		return str
			.trim()
			.match(/[a-z,0-9]+/gi)
			.map(word => word.charAt(0).toUpperCase() + word.substr(1).toLowerCase())
			.join("");
	}

	private identifyObjectsRecursively(data: unknown, name: string): ORASS.Object[] {
		if (isSingleValued(getType(data)) || this.isEmpty(data)) {
			return [];
		}

		let objects: ORASS.Object[] = [];

		if (isObject(getType(data))) {
			objects.push({
				name,
				body: <ORASS.ObjectBody>data,
			});
			objects = objects.concat(Object.keys(data)
				.flatMap(key => this.identifyObjectsRecursively(data[key], this.pascalize(key))));
		} else if (isMultiValued(getType(data))) {
			objects = objects.concat((<Array<unknown>>data)
				.flatMap(item => this.identifyObjectsRecursively(item, this.pascalize(name + ORASS.ARRAY_ITEM_POSTFIX))));
		}

		return objects.filter(object => !!object.body);
	}

	private reduceObjectClasses(oClasses: ORASS.ObjectClass[]): ORASS.ObjectClass[] {
		const chunkSize = 100;
		const reducedOClasses = [];
		for (let i = 0; i < oClasses.length; i += chunkSize) {
			reducedOClasses.push(...this.reduceChunkOfObjectClasses(oClasses.slice(i, Math.min(oClasses.length, i + chunkSize))));
		}
		return this.reduceChunkOfObjectClasses(reducedOClasses);
	}

	/**
	 * needed for decreasing the enormously long time spent when there are too many extracted object classes.
	 * the problem must be with Array.prototype.splice method, which modifies the array.
	 */
	private reduceChunkOfObjectClasses(oClasses: ORASS.ObjectClass[]): ORASS.ObjectClass[] {
		const reducedOClasses = [...oClasses];
		for (let i = 1; i < reducedOClasses.length; ++i) {
			for (let j = 0; j < i; ++j) {
				if (reducedOClasses[j].name === reducedOClasses[i].name) {
					reducedOClasses[i] = this.mergeObjectClasses(reducedOClasses[j], reducedOClasses[i]);
					reducedOClasses.splice(j, 1);
					i--;
					j--;
				}
			}
		}
		return reducedOClasses;
	}

	private mergeObjectClasses(first: ORASS.ObjectClass, second: ORASS.ObjectClass): ORASS.ObjectClass {
		return new ORASS.ObjectClass(
			first.name,
			[...new Set(first.instances.concat(second.instances))],
			[...this.mergeAttrs(first.attrs, second.attrs)],
			[...new Set(first.objectClasses.concat(second.objectClasses))],
		);
	}

	private mergeAttrs(first: ORASS.Attribute[], second: ORASS.Attribute[]): ORASS.Attribute[] {
		const attrs1 = [...first];
		const attrs2 = [...second];
		const result: ORASS.Attribute[] = [];

		for (let attr1 of attrs1) {
			const attr2 = attrs2.find(val2 => val2.name === attr1.name);

			if (!attr2) {
				attr1.required = false;
				result.push(attr1);
				continue;
			}

			result.push(new ORASS.Attribute(
				attr1.name,
				this.mergeValueTypes(attr1.valueType, attr2.valueType),
				[...new Set(attr1.examples.concat(attr2.examples))],
				attr1.required && attr2.required,
			));

			attrs2.splice(attrs2.indexOf(attr2), 1);
		}

		attrs2.forEach(attr2 => attr2.required = false);

		result.push(...attrs2);

		return result;
	}

	private mergeValueTypes(type1: ValueType, type2: ValueType): ValueType {
		let type: ValueType;

		if (!type1 && !type2) {
			type = ValueType.null;
		} else if (!type1) {
			type = type2;
		} else if (!type2) {
			type = type1;
		} else if (type1 === type2) {
			type = type1;
		} else {
			type = ValueType.any;
		}

		return type;
	}

	private classifyAttrs(body: ORASS.ObjectBody): ORASS.Attribute[] {
		const attributes: ORASS.Attribute[] = [];

		for (let key in body) {
			if (isSingleValued(getType(body[key])) ||
				(isMultiValued(getType(body[key])) && (<unknown[]>body[key]).every(item => isSingleValued(getType(item))))
			) {
				attributes.push(new ORASS.Attribute(
					key,
					getType(body[key]),
					[body[key]],
				));
			}
		}

		return attributes;
	}

	/**
	 * WARNING! objectClasses mustn't repeat
	 */
	private getNode<T extends ORASS.Node>(name: string, nodes: T[]): T {
		return nodes.find(node => node.name === this.pascalize(name));
	}
}

