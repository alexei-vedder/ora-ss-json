import { Test, TestingModule } from "@nestjs/testing";
import { SerializingService } from "./serializing.service";
import { GraphLinksModel } from "gojs";
import { ORASS } from "../models/ora-ss.model";
import { NodeType, ValueType } from "../models/type.enum";

const NODES_1 = {
	"1": new ORASS.ObjectClass("1"),
	"2": new ORASS.ObjectClass("2"),
	"3": new ORASS.ObjectClass("3"),
	"4": new ORASS.ObjectClass("4"),
	"1-1": new ORASS.Attribute("1-1", ValueType.string, [], true, true),
	"1-2": new ORASS.Attribute("1-2", ValueType.array),
	"4-1": new ORASS.Attribute("4-1", ValueType.string, [], true, true),
	"4-2": new ORASS.Attribute("4-2", ValueType.string, [], true),
};

const SCHEMA_1 = new GraphLinksModel(
	Object.keys(NODES_1).map(key => NODES_1[key]),
	[
		{ from: NODES_1["1"].key, to: NODES_1["2"].key },
		{ from: NODES_1["1"].key, to: NODES_1["1-1"].key },
		{ from: NODES_1["1"].key, to: NODES_1["1-2"].key },
		{ from: NODES_1["1-2"].key, to: NODES_1["4"].key },
		{ from: NODES_1["2"].key, to: NODES_1["3"].key },
		{ from: NODES_1["4"].key, to: NODES_1["4-1"].key },
		{ from: NODES_1["4"].key, to: NODES_1["4-2"].key },
		{ from: NODES_1["4-2"].key, to: NODES_1["1"].key },
	],
);


describe("SerializingService", () => {
	let service: SerializingService;
	let serialized: string;
	let deserialized: ORASS.Schema;

	beforeEach(async () => {
		const module: TestingModule = await Test.createTestingModule({
			providers: [SerializingService],
		}).compile();

		service = module.get<SerializingService>(SerializingService);
	});

	it("should be defined", () => {
		expect(service).toBeDefined();
	});

	describe("Serializing and deserializing of SCHEMA_1", () => {

		let objectClasses;
		let attributes;

		beforeAll(() => {
			serialized = service.serialize(SCHEMA_1);
			deserialized = service.deserialize(serialized);
			objectClasses = deserialized.nodeDataArray.filter(node => node.type === NodeType.ObjectClass);
			attributes = deserialized.nodeDataArray.filter(node => node.type === NodeType.Attribute);
		});

		it("serialized and deserialized must be defined", () => {
			// console.log("Serialized", serialized);
			expect(serialized).toBeDefined();
			// console.log("Deserialized", deserialized.toJson());
			expect(deserialized).toBeDefined();

			expect(deserialized.nodeDataArray).toHaveLength(SCHEMA_1.nodeDataArray.length);
			expect(deserialized.linkDataArray).toHaveLength(SCHEMA_1.linkDataArray.length);
		});

		it("deserialized must have all ORASS.Node instances of initial type", () => {

			const initialObjectClasses = Object.keys(NODES_1)
				.filter(key => NODES_1[key] instanceof ORASS.ObjectClass)
				.map(key => NODES_1[key]);

			const initialAttributes = Object.keys(NODES_1)
				.filter(key => NODES_1[key] instanceof ORASS.Attribute)
				.map(key => NODES_1[key]);

			expect(objectClasses).toHaveLength(initialObjectClasses.length);
			expect(attributes).toHaveLength(initialAttributes.length);

			const keys: string[] = objectClasses.map(oClass => oClass.key).concat(attributes.map(attr => attr.key));

			objectClasses.forEach(objectClass => {
				expect(objectClass).toBeInstanceOf(ORASS.ObjectClass);
				expect(keys.includes(objectClass.key)).toBeTruthy();
			});

			attributes.forEach(attr => {
				expect(attr).toBeInstanceOf(ORASS.Attribute);
				expect(keys.includes(attr.key)).toBeTruthy();
			});
		});

		it("links of each ObjectClass must be restored", () => {
			deserialized.linkDataArray.forEach(link => {
				const source = objectClasses.find(node => node.key === link.from);
				const target = deserialized.nodeDataArray.find(node => node.key === link.to);
				expect(target).toBeDefined();
				if (source) {
					if (target instanceof ORASS.ObjectClass) {
						expect(source.objectClasses).toBeDefined();
						expect(source.objectClasses.find(oClass => oClass.key === link.to)).toBe(target);
					} else if (target instanceof ORASS.Attribute) {
						expect(source.attrs).toBeDefined();
						expect(source.attrs.find(attr => attr.key === link.to)).toBe(target);
					}
				}
			});
		});

		it("IDREFs of each Attribute must be restored", () => {
			deserialized.linkDataArray.forEach(link => {
				const source = attributes.find(node => node.key === link.from);
				const target = deserialized.nodeDataArray.find(node => node.key === link.to);
				expect(target).toBeDefined();
				if (source) {
					if (target instanceof ORASS.ObjectClass) {
						expect(source.target).toBe(target);
					}
				}
			});
		});
	});

	/**
	 * This test group proves that gojs.GraphLinkModel.prototype.toJSON(..)
	 * cannot serialize nodes with circular pointers,
	 * and because of this fact SerializingService is needed
	 */
	describe("Serializing nodes with circular pointers", () => {
		const node1 = new ORASS.ObjectClass("1");
		const node2 = new ORASS.ObjectClass("2");
		const node3 = new ORASS.Attribute("3", ValueType.string);

		node1.objectClasses.push(node2);
		node2.attrs.push(node3);
		node3.idref = node1;

		const graph = new GraphLinksModel(
			[node1, node2, node3],
			[
				{ from: "1", to: "2" },
				{ from: "2", to: "3" },
				{ from: "3", to: "1" },
			],
		);

		it("must serialize successfully", () => {
			const serialize = jest.fn(() => {
				let serialized;
				try {
					serialized = service.serialize(graph);
				} catch (error) {
					throw error;
				}
				return serialized;
			});

			serialize();

			expect(serialize).toHaveReturned();
		});

		it("Serializing via gojs.Model.prototype.toJson(...) must throw RangeError", () => {
			const serialize = () => {
				let serialized;
				try {
					serialized = graph.toJson();
				} catch (error) {
					throw error;
				}
				return serialized;
			};

			expect(serialize).toThrow(RangeError);
		});
	});
});
