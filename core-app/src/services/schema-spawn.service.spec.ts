import { Test, TestingModule } from "@nestjs/testing";
import { NodeHandlingService } from "./node-handling.service";
import { EventCallbacks, SchemaSpawnService } from "./schema-spawn.service";
import { ORASS } from "../models/ora-ss.model";
import { NodeType, ValueType } from "../models/type.enum";
import { SerializingService } from "./serializing.service";

const BOOK_EXAMPLE_DATA = {
	courses: [
		{
			code: "CS1102",
			title: "Data Structure",
			differentTypedAttr: "string type",
			notRequiredAttr: "abcd",
			notRequiredMultiAttr: ["abcd", "efgh"],
			students: [
				{
					stuNo: "stu123",
					stuName: "Zheng Zhang",
					address: "hostel 01 - 03 - A",
					grade: "A",
					tutor: {
						staffNo: "stf101",
						office: "S17 - 05 - 07",
						feedback: "Very interesting course",
					},
				},
				{
					stuNo: "stu125",
					stuName: "Liang Chen",
					address: "hostel 01 - 04 - C",
					hobbies: [
						"hockey",
						"reading",
					],
					grade: "B",
					tutor: {
						staffNo: "stf101",
						office: "S17 - 05 - 07",
						feedback: "a bit difficult",
					},
				},
				{
					stuNo: "stu126",
					stuName: "Xiaochun Wang",
					address: "hostel 02 - 04 - A",
					hobbies: [
						"swimming",
						"tennis",
					],
					grade: "A+",
					tutor: {
						staffNo: "stf102",
						office: "S15 - 06 - 03",
						feedback: "learnt a lot in this course",
					},
				},
				{
					stuNo: "stu130",
					stuName: "Mingming Liu",
					address: "hostel 03 - 01 - F",
					grade: "B+",
					tutor: {
						staffNo: "stf102",
						office: "S15 - 06 - 03",
						feedback: "boring",
					},
				},
			],
			lecturer: {
				staffNo: "stf001",
				staffName: "Jie Xu",
				office: "S17 - 04 - 25",
				teaching: ["CS1102"],
			},
		},
		{
			code: "CS2104",
			title: "Programming Concepts",
			differentTypedAttr: ["array type"],
			students: [
				{
					stuNo: "stu123",
					stuName: "Zheng Zhang",
					address: "hostel 01 - 03 - A",
					grade: "B",
					tutor: {
						staffNo: "stf101",
						office: "S17 - 05 - 07",
						feedback: "approachable tutor",
					},
				},
				{
					stuNo: "stu125",
					stuName: "Liang Chen",
					address: "hostel 01 - 04 - C",
					hobbies: [
						"hockey",
						"reading",
					],
					grade: "A",
					tutor: {
						staffNo: "stf101",
						office: "S17 - 05 - 07",
						feedback: "friendly and approachable",
					},
				},
				{
					stuNo: "stu126",
					stuName: "Xiaochun Wang",
					address: "hostel 02 - 04 - A",
					hobbies: [
						"swimming",
						"tennis",
					],
					grade: "B-",
					tutor: {
						staffNo: "stf101",
						office: "S17 - 05 - 07",
						feedback: "helpful",
					},
				},
			],
			lecturer: {
				staffNo: "stf002",
				staffName: "Dachuan Zhou",
				office: "S15 - 03 - 15",
				teaching: ["CS1102", "CS2104"],
			},
		},
	],
};

const IDREFFUL_EXAMPLE = {
	mains: [
		{
			id: "123",
			idref: "abc",
		}, {
			id: "345",
			idref: "abc",
		},
	],
	second: {
		id: "abc",
		idref: "xyz",
	},
	third: {
		num: "xyz",
	},
	fourth: {
		name: "obc",
		multiIdref: ["123", "345"],
	},
};

describe("SchemaSpawnService", () => {
	let service: SchemaSpawnService;
	let nodeHandler: NodeHandlingService;

	beforeEach(async () => {
		const module: TestingModule = await Test.createTestingModule({
			providers: [
				SchemaSpawnService,
				NodeHandlingService,
				SerializingService,
			],
		}).compile();

		service = await module.get<SchemaSpawnService>(SchemaSpawnService);
		nodeHandler = await module.get<NodeHandlingService>(NodeHandlingService);
	});

	it("must be defined", () => {
		expect(service).toBeDefined();
	});

	describe("spawnSchema(...)", () => {

		let schema: ORASS.Schema;

		let eventCallbacks: EventCallbacks = {
			requestExtraInput: ({ options }) => Promise.resolve({ answer: [options[0]] }),
			sendNotification: console.log,
			reportProgress: console.log,
		};

		it("Spawning schema for the Book Example Data", async () => {
			schema = await service.spawnSchema(BOOK_EXAMPLE_DATA, eventCallbacks);
			expect(schema).toBeDefined();

			const objectClasses = schema.nodeDataArray.filter(node => node.type === NodeType.ObjectClass) as ORASS.ObjectClass[];
			expect(objectClasses).toHaveLength(5);

			const nonObjectClassAttrs = objectClasses
				.flatMap(oClass => oClass.attrs.filter(attr => attr.nonObjectClassAttr));

			expect(nonObjectClassAttrs).toHaveLength(2);

			const differentTypedAttr = schema.nodeDataArray
				.filter(node => node.type === NodeType.Attribute)
				.find(node => node.name === "differentTypedAttr") as ORASS.Attribute;

			expect(differentTypedAttr).toBeDefined();
			expect(differentTypedAttr.valueType).toEqual(ValueType.any);

		});

		it("Spawning schema for Idrefful Example Data", async () => {
			schema = await service.spawnSchema(IDREFFUL_EXAMPLE, eventCallbacks);
			expect(schema).toBeDefined();

			const objectClasses = schema.nodeDataArray.filter(node => node.type === NodeType.ObjectClass) as ORASS.ObjectClass[];
			expect(objectClasses).toHaveLength(5);
			const oClassesWithIdrefs = objectClasses.filter(oClass => oClass.idrefAttrs.length !== 0);
			expect(oClassesWithIdrefs).toHaveLength(3);
			oClassesWithIdrefs.forEach(oClass => {
				expect(oClass).toBeDefined();
			});

			const nonObjectClassAttrs = objectClasses
				.map(oClass => oClass.attrs.filter(attr => attr.nonObjectClassAttr))
				.flat();

			expect(nonObjectClassAttrs).toHaveLength(0);
		});
	});
});
