import { Injectable } from "@nestjs/common";
import { GraphLinksModel } from "gojs";
import { ORASS } from "../models/ora-ss.model";
import { NodeHandlingService } from "./node-handling.service";
import { ExtraInput, Notification, ProcessingProgress } from "../../../shared-models/web-socket-events.model";
import { isDeepStrictEqual } from "util";
import { depthFirstSearch } from "../fns/depth-first-search";
import { isAny, isMultiValued, isSingleValued } from "../fns/value-type-validators";
import { transpose } from "../fns/transpose";


export interface EventCallbacks {
	requestExtraInput: (responseBody: ExtraInput.ResponseBody) => Promise<ExtraInput.RequestBody>;
	sendNotification: (responseBody: Notification.ResponseBody) => void;
	reportProgress: (responseBody: ProcessingProgress.ResponseBody) => void;
}

@Injectable()
export class SchemaSpawnService {

	private readonly MAX_INPUT_REQUEST_EXAMPLES = 16;

	constructor(private nodeHandler: NodeHandlingService) {
	}

	public async spawnSchema(data: unknown, callbacks: EventCallbacks): Promise<ORASS.Schema> {

		console.time("Rough schema spawn");

		callbacks.reportProgress({ task: "Refining data", value: 10 });
		const refinedData = this.nodeHandler.nullify(data);

		callbacks.reportProgress({ task: "Spawning rough schema", value: 20 });
		const schema = this.spawnRoughSchema(refinedData);

		const objectClasses: ORASS.ObjectClass[] =
			schema.nodeDataArray
				.filter(node => node instanceof ORASS.ObjectClass) as ORASS.ObjectClass[];

		console.timeEnd("Rough schema spawn");

		try {
			callbacks.reportProgress({ task: "Resolving object class ids", value: 40 });
			await this.resolveObjectClassIds(objectClasses, callbacks);
			callbacks.reportProgress({ task: "Resolving IDREFs", value: 50 });
			await this.resolveIdrefs(objectClasses, callbacks);
		} catch (e) {
			callbacks.sendNotification({
				message: e.message + "\nRelationship types haven't been spawned!",
				type: Notification.Type.Error,
			});
			return schema;
		}

		objectClasses.forEach(oClass => {
			oClass.idrefAttrs.forEach(idrefAttr => {
				schema.addLinkData({ from: idrefAttr.key, to: idrefAttr.target.key });
			});
		});

		callbacks.reportProgress({ task: "Finding relationship types data", value: 70 });
		this.findRelationshipTypes(schema, objectClasses);

		callbacks.reportProgress({ task: "Schema spawned", value: 100 });
		return schema;
	}

	private spawnRoughSchema(data: unknown): ORASS.Schema {
		const objects = this.nodeHandler.identifyObjects(data);
		// console.log("OBJECTS:", inspect(objects, true, null, true));

		const objectClasses = this.nodeHandler.identifyObjectClasses(objects);
		// console.log("OBJECT CLASSES:", inspect(objectClasses, true, 4, true));

		const roughSchema: ORASS.Schema = new GraphLinksModel();

		objectClasses.forEach(oClass => {
			roughSchema.addNodeData(oClass);
			oClass.attrs.forEach(attr => {
				roughSchema.addNodeData(attr);
				roughSchema.addLinkData({ from: oClass.key, to: attr.key });
			});
			oClass.objectClasses.forEach(referredOClass => {
				roughSchema.addLinkData({ from: oClass.key, to: referredOClass?.key });
			});
		});

		return roughSchema;
	}

	private findRelationshipTypes(schema: ORASS.Schema, objectClasses: ORASS.ObjectClass[]): void {
		this.resolveNonObjectClassAttributes(objectClasses);

		const relationshipTypes = this.spawnRelationshipTypes(schema);

		//console.log("RTs:", inspect(relationshipTypes, true, 5));

		relationshipTypes.forEach(rt => {
			const link = schema.linkDataArray.find(link => (link.from === rt.path[rt.path.length - 2]) && (link.to === rt.path[rt.path.length - 1]));
			link["relationshipType"] = rt;
		});
	}

	// TODO optimize, too much of deepEqual calls
	private resolveNonObjectClassAttributes(objectClasses: ORASS.ObjectClass[]): void {
		for (let oClass of objectClasses) {
			const uniqueIds: unknown[][] = this.nodeHandler.extractObjectClassUniqueIdValues(oClass);

			for (let uniqueId of uniqueIds) {
				const instancesWithSameId = oClass.instances.filter(instance => {
					const id = this.nodeHandler.getIdentifierValue(oClass, instance);
					return isDeepStrictEqual(uniqueId, id);
				});

				const nonIdAttrs = oClass.attrs.filter(attr => !attr.isId);

				nonIdAttrs.forEach(attr => {
					const uniqueAttrValuesAmongInstancesWithSameId
						= new Set(instancesWithSameId.map(instance => JSON.stringify(instance.body[attr.name])));
					if (uniqueAttrValuesAmongInstancesWithSameId.size !== 1) {
						attr.nonObjectClassAttr = true;
					}
				});
			}
		}
	}

	private async resolveIdrefs(objectClasses: ORASS.ObjectClass[],
								{ requestExtraInput }: EventCallbacks): Promise<void> {
		for (let oClass of objectClasses) {
			const nonIdAttrs = oClass.attrs.filter(attr => !attr.isId);
			for (let potentialIdrefAttr of nonIdAttrs) {
				for (let targetOClass of objectClasses) {

					/*console.log(
						"oClass:", inspect(oClass),
						"\nPotential IDREF attr:", inspect(potentialIdrefAttr),
						"\nTarget:", inspect(targetOClass));*/

					if (targetOClass === oClass) {
						continue;
					}

					if (targetOClass.identifier.length === 0) {
						continue;
					}

					if (1 < targetOClass.identifier.length) {
						console.warn(
							"Unable to find idref in class with composite identifier. Identifier:",
							targetOClass.identifier,
						);
						continue;
					}

					let isPotentialIdref: boolean;

					if (isSingleValued(potentialIdrefAttr.valueType)) {
						isPotentialIdref = potentialIdrefAttr.examples.reduce((isIdref, example) =>
							(isIdref && targetOClass.identifier[0].examples.includes(example)), true);

					} else if (isMultiValued(potentialIdrefAttr.valueType)) {
						isPotentialIdref = potentialIdrefAttr.examples
							.reduce((isMultiIdref: boolean, example: any[]) =>
								example.reduce((isIdref, exampleElem) =>
									(isIdref && targetOClass.identifier[0].examples.includes(exampleElem)), isMultiIdref), true);

					} else if (isAny(potentialIdrefAttr.valueType)) {
						isPotentialIdref = false;
					} else {
						throw new Error(`Unable to handle an attribute of type ${potentialIdrefAttr.valueType}`);
					}

					if (isPotentialIdref) {

						const { answer } = await requestExtraInput({
							message: `Does the attribute <strong><code>${potentialIdrefAttr.name}</code></strong> of <strong><code>${oClass.name}</code></strong> actually refer to <strong><code>${targetOClass.name}</code></strong> ?`,
							options: ["Yes", "No"],
							examples: [
								...oClass.instances.slice(0, Math.min(oClass.instances.length, this.MAX_INPUT_REQUEST_EXAMPLES / 2)),
								...targetOClass.instances.slice(0, Math.min(targetOClass.instances.length, this.MAX_INPUT_REQUEST_EXAMPLES / 2))
							],
							mode: ExtraInput.SelectMode.One,
						});

						if (answer[0] === "Yes") {
							potentialIdrefAttr.idref = targetOClass;
							// if idref found, there is no point to seek for the rest object classes, we've already found one
							break;
						}
					}
				}
			}
		}
	}

	private async resolveObjectClassIds(objectClasses: ORASS.ObjectClass[],
										{ requestExtraInput }: EventCallbacks): Promise<void> {
		for (let objectClass of objectClasses) {

			const potentialIds = objectClass.singleAttrs.filter(attr => attr.required);

			if (potentialIds.length === 0) {
				continue;
			}

			if (potentialIds.length === 1) {
				objectClass.attrs.find(attr => attr.name === potentialIds[0].name).isId = true;
				continue;
			}

			const { answer: identifier } = await requestExtraInput({
				message: `Which attribute should be considered as identifier of the object <strong><code>${objectClass.name}</code></strong>?`,
				options: potentialIds.map(attr => attr.name),
				examples: objectClass.instances
					.slice(0, Math.min(objectClass.instances.length, this.MAX_INPUT_REQUEST_EXAMPLES))
					.map(instance => instance.body),
				mode: ExtraInput.SelectMode.AtLeastOne,
			});

			if (!Array.isArray(identifier)) {
				throw new Error("Response is invalid. There are no 'chosenOptions' at all");
			}

			if (identifier.length === 0) {
				throw new Error("No chosen options to identify the object class identifier");
			}

			objectClass.attrs
				.filter(attr => identifier.includes(attr.name))
				.forEach(idAttr => idAttr.isId = true);
		}
	}

	private spawnRelationshipTypes(schema: ORASS.Schema): ORASS.RelationshipType[] {

		const relationshipTypes: ORASS.RelationshipType[] = [];
		const tables = this.spawnRelationshipTypeTables(schema);

		for (let table of tables) {

			if (table.nonOCAttrs.length === 0) {
				console.warn("Functionality to create relationship type without related attributes is not supported yet"); // TODO
				continue;
			}

			const parentKeys = table.leafToRootPath.slice(1);

			parentKeys.reduce((idColumns: any[][], parentKey: string, parentKeyIndex: number, parentKeys: string[]) => {

				idColumns = idColumns.concat(table.getParentColumns(parentKeyIndex));

				const sameTuplesIndexes = this.findSameTuplesIndexes(idColumns);

				for (let nonOCAttrColumn of table.nonOCAttrsColumns) {

					const isDependent: boolean = sameTuplesIndexes.reduce((isDependent, sameTupleIndexes) => {
						const expectedValue = nonOCAttrColumn[sameTupleIndexes[0]];
						return isDependent && sameTupleIndexes.every(sameTupleIndex => nonOCAttrColumn[sameTupleIndex] === expectedValue);
					}, true);

					if (isDependent) {

						relationshipTypes.push({
							path: table.leafToRootPath.reverse(),
							name: table.leafToRootPath.map(id => schema.nodeDataArray.find(node => node.key === id).name).join("-"),
							degree: parentKeyIndex + 2,
							parentParticipationConstraint: this.findParticipationConstraints(table.getParentColumns(0), table.leafColumns),
							childParticipationConstraint: this.findParticipationConstraints(table.leafColumns, table.getParentColumns(0)),
							attribute: table.nonOCAttrs[table.nonOCAttrsColumns.indexOf(nonOCAttrColumn)],
						});

						// to break from reduce
						parentKeys.splice(parentKeyIndex);
						break;
					}
				}

				return idColumns;

			}, table.leafColumns);
		}

		return relationshipTypes;
	}

	private findParticipationConstraints(first: any[][], second: any[][]): [number, number] {
		const tuples = transpose(second);
		return this.findSameTuplesIndexes(first).reduce((participationConstraint: [number, number], indexes: number[]) => {
			const participation = new Set(indexes.map(index => JSON.stringify(tuples[index]))).size;
			return [Math.min(participation, participationConstraint[0]), Math.max(participation, participationConstraint[1])];
		}, [Infinity, 0]) as [number, number];
	}

	/**
	 *	e.i. input as follows:
	 *	```
	 *  [["0", "1", "1", "3", "0", "1"],
	 *   ["9", "8", "8", "5", "9", "8"]]
	 *  ```
	 * 	response will be this:
	 * 	```
	 * 	[[0,4], [1,2,5], [3]]
	 * 	```
	 * unique tuple 0 is placed in [0, 4] positions of the array of non-unique tuples
	 * unique tuple 1 is placed in [1, 2, 5] positions
	 * unique tuple 2 is placed in [3] position
	 * @param columns
	 */
	private findSameTuplesIndexes(columns: any[][]): number[][] {
		const idTuples = transpose(columns);

		const uniqueStringifiedIdTuples = [...new Set(idTuples.map(tuple => JSON.stringify(tuple)))];
		return uniqueStringifiedIdTuples.map(uniqueTuple => {
			const sameTupleIndexes: number[] = [];
			idTuples.forEach((idTuple, index) => {
				if (JSON.stringify(idTuple) === uniqueTuple) {
					sameTupleIndexes.push(index);
				}
			});
			return sameTupleIndexes;
		});
	}

	private spawnRelationshipTypeTables(schema: ORASS.Schema): ORASS.RelationshipTypeTable[] {
		const leafToRootPaths = this.findLeafToRootPaths(schema);

		if (!leafToRootPaths.length) {
			return [];
		}

		const tables: ORASS.RelationshipTypeTable[] = [];

		leafToRootPaths.forEach(path => {
			const leaf = schema.findNodeDataForKey(path[0]) as ORASS.ObjectClass;
			if (leaf) {
				tables.push(this.initRTTable(schema, path, leaf));
			}
		});

		const restNonObjectClassAttrs = schema.nodeDataArray
			.filter(node => node instanceof ORASS.ObjectClass)
			.flatMap((oClass: ORASS.ObjectClass) => oClass.nonObjectClassAttrs)
			.filter(attr => !tables
				.flatMap(table => table.head)
				.filter(attr => attr.nonObjectClassAttr)
				.includes(attr));

		restNonObjectClassAttrs.forEach(attr => {
			const oClassKey = schema.linkDataArray.find(link => link.to === attr.key).from;

			let path = leafToRootPaths.find(path => path.includes(oClassKey));
			path = path.slice(path.indexOf(oClassKey));

			// used as a leaf but it is actually not
			const child = schema.findNodeDataForKey(path[0]) as ORASS.ObjectClass;

			tables.push(this.initRTTable(schema, path, child, [attr]));
		});

		return tables;
	}

	private initRTTable(schema: ORASS.Schema, path: string[], leaf: ORASS.ObjectClass, nonOCAttrs?: ORASS.Attribute[]): ORASS.RelationshipTypeTable {

		const _nonOCAttrs = nonOCAttrs || leaf.nonObjectClassAttrs;
		const table = new ORASS.RelationshipTypeTable(path, leaf.identifier, _nonOCAttrs);

		table.head.forEach(attr => {
			const oClassKey = table.getObjectClassKey(attr.key);
			const attrValues = schema.findNodeDataForKey(oClassKey).instances
				.map(instance => instance.body[attr.name]);
			table.setColumn(attr.key, attrValues);
		});

		const parentKeys = path.slice(1);
		this.fillRTTableWithParents(schema, table, parentKeys, leaf);

		return table;
	}

	private fillRTTableWithParents(schema: ORASS.Schema, table: ORASS.RelationshipTypeTable, parentKeys: string[], leaf: ORASS.ObjectClass): void {

		for (let parentKey of parentKeys) {
			const parent = schema.findNodeDataForKey(parentKey) as ORASS.ObjectClass;
			table.setParentId(parent.identifier);

			parent.identifier.forEach(parentIdPart => {
				table.setColumn(parentIdPart.key, []);
			});

			// filling table body with parent id values
			// point out that new parent values are extracted for each LEAF id tuple,
			// intermediate parents don't participate in this process

			const leafIdTuples = transpose(table.leafColumns);

			leafIdTuples.forEach((leafIdTuple, leafIdTupleIndex) => {

				const leafInstance = this.findObjectClassInstancesByIdTuple(leaf, leafIdTuple)
					.find(leafInstance => table.nonOCAttrs
						.reduce((suit, attr, index) =>
							suit && isDeepStrictEqual(leafInstance.body[attr.name], table.nonOCAttrsColumns[index][leafIdTupleIndex]), true));

				const parentInstance = this.findParentInstanceContainingLeafInstance(parent, leafInstance);

				const parentKeyIndex = parentKeys.indexOf(parentKey);

				const parentIdAttrNames = table.getParentIdentifier(parentKeyIndex).map(attr => attr.name);

				table.getParentColumns(parentKeyIndex).forEach((parentIdPartValues, parentIdPartIndex) => {
					parentIdPartValues.push(parentInstance.body[parentIdAttrNames[parentIdPartIndex]]);
				});
			});
		}
	}

	private findObjectClassInstancesByIdTuple(oClass: ORASS.ObjectClass, idTuple: any[]) {
		return oClass.instances.filter(instance => {

			const instanceId = Object.keys(instance.body)
				.filter(key => oClass.identifier
					.map(idPart => idPart.name)
					.includes(key))
				.map(key => instance.body[key]);

			return idTuple.every(idPart => JSON.stringify(instanceId).includes(idPart));
		});
	}

	private findParentInstanceContainingLeafInstance(parent: ORASS.ObjectClass, leafInstance: ORASS.Object): ORASS.Object {
		return parent.instances
			.find(parentInstance => JSON.stringify(parentInstance.body)
				.includes(JSON.stringify(leafInstance.body)));
	}

	private findLeafToRootPaths(schema: ORASS.Schema): string[][] {

		const attrlessSchema = this.nodeHandler.removeAttributes(schema);

		const rootToLeafPaths: string[][] = [];
		const currentPath: string[] = [];
		let lastEntered: string;

		const enterNode = (currentNodeKey) => {
			currentPath.push(currentNodeKey);
			lastEntered = currentNodeKey;
		};

		const leaveNode = (currentNodeKey) => {
			if (lastEntered === currentNodeKey) {
				rootToLeafPaths.push([...currentPath]);
			}
			currentPath.pop();
		};

		depthFirstSearch(
			attrlessSchema.linkDataArray as { from: string, to: string }[],
			enterNode,
			leaveNode,
		);

		return rootToLeafPaths.map(path => path.reverse());
	}
}
