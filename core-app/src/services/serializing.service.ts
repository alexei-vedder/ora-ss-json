import { Injectable } from "@nestjs/common";
import { ORASS } from "../models/ora-ss.model";
import { GraphLinksModel } from "gojs";
import { NodeType } from "../models/type.enum";

@Injectable()
export class SerializingService {

	public serialize(schema: ORASS.Schema): string {
		const nodes: Object[] =
			(schema.nodeDataArray as ORASS.Node[])
				.map(node => JSON.parse(node.toJSON()));

		return new GraphLinksModel([...nodes], [...schema.linkDataArray]).toJson();
	}

	public deserialize(serialized: string): ORASS.Schema {
		const schemaWithPlainNodes: ORASS.Schema = GraphLinksModel.fromJson(serialized) as ORASS.Schema;

		const instantiatedNodes: ORASS.Node[] =
			this.instantiateNodes(schemaWithPlainNodes.nodeDataArray);

		instantiatedNodes
			.forEach(node =>
				node.restoreLinks(instantiatedNodes, schemaWithPlainNodes.linkDataArray as { from, to }[]));

		return new GraphLinksModel(instantiatedNodes, schemaWithPlainNodes.linkDataArray);
	}

	private instantiateNodes(nodeDataArray) {
		return nodeDataArray.map(node => {
			if (node.type === NodeType.ObjectClass) {
				return ORASS.ObjectClass.fromJSON(JSON.stringify(node));
			} else if (node.type === NodeType.Attribute) {
				return ORASS.Attribute.fromJSON(JSON.stringify(node));
			} else {
				throw new Error("Unable to handle this node: " + JSON.stringify(node));
			}
		});
	}
}
