import { Test, TestingModule } from "@nestjs/testing";
import { ParsingService } from "./parsing.service";
import { map, toArray } from "rxjs/operators";


const DATA = {
	a: "b",
	c: [1, 2, 3],
	d: {
		e: true,
		g: null,
	},
};
const DATA_AS_STRING = JSON.stringify(DATA);
const DATA_AS_BUFFER = Buffer.from(DATA_AS_STRING);
const URL = "https://jsonplaceholder.typicode.com";

describe("ParsingService", () => {
	let service: ParsingService;

	beforeEach(async () => {
		const module: TestingModule = await Test.createTestingModule({
			providers: [ParsingService],
		}).compile();

		service = module.get<ParsingService>(ParsingService);
	});

	it("should be defined", () => {
		expect(service).toBeDefined();
	});

	describe("extractData(..)", () => {
		it("must return array of valid objects by URLs", async () => {
			const commentsURL = URL + "/comments";
			const usersURL = URL + "/users";
			const dataFromStringURLs = await service.extractData([
				commentsURL,
				usersURL,
			]);
			const dataFromBufferURLs = await service.extractData([
				Buffer.from(commentsURL),
				Buffer.from(usersURL),
			]);

			expect(dataFromStringURLs).toHaveLength(2);
			expect(dataFromBufferURLs).toHaveLength(2);
			expect(dataFromStringURLs).toEqual(dataFromBufferURLs);
		});
	});

	describe("parse(..)", () => {
		it("must return valid data", async () => {
			const parsedString = await service.parse(DATA_AS_STRING, 10);
			const parsedBuffer = await service.parse(DATA_AS_BUFFER, 10);
			expect(parsedString).toEqual(DATA);
			expect(parsedBuffer).toEqual(DATA);
		});
	});

	describe("parseStream(..) and load(..)", () => {
		it("must load by URL and return valid data", async () => {
			const parsedBuffer = await service.parseStream(service.load(URL + "/users"));
			const parsedString = await service.parseStream(service.load(URL + "/users").pipe(map(chunk => chunk.toString("utf-8"))));
			expect(parsedBuffer).toBeDefined();
			expect(parsedString).toBeDefined();
			expect(parsedString).toEqual(parsedBuffer);
		});
	});

	describe("stringify(..)", () => {
		it("must return valid string", async () => {
			const stringifiedData = await service.stringify(DATA);
			expect(stringifiedData).toEqual(DATA_AS_STRING);
		});
	});

	describe("stringifyStream(..)", () => {
		it("must return valid string", () => {
			service.stringifyStream(DATA)
				.pipe(toArray())
				.subscribe((chunks: string[]) => {
					expect(chunks.join("")).toEqual(DATA_AS_STRING);
				});
		});
	});

});
