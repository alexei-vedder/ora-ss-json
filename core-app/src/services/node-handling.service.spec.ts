import { Test, TestingModule } from "@nestjs/testing";
import { NodeHandlingService } from "./node-handling.service";
import { ORASS } from "../models/ora-ss.model";
import { NodeType, ValueType } from "../models/type.enum";
import { SerializingService } from "./serializing.service";


const DATA_1 = [[
	{
		"a12": 1,
		"a13": 2,
		"a14": [[[
			{
				"a21": 1,
				"a22": 2,
				"a23": {
					"a31": 5,
					"a32": 6,
				},
			},
			{
				"a21": 1,
				"a22": 2,
			},
			{
				"a21": 1,
				"a22": 2,
				"a23": {
					"a31": 50,
					"a32": 60,
				},
			},
		]]],
	},
]];

const OBJECTS_1: ORASS.Object[] = [
	{
		name: "X",
		body: {
			a: "2",
			b: "3",
			c: 4,
			obj: {
				subObj: {
					f: "1",
					s: "8",
				},
			},
		},
	},
	{
		name: "X",
		body: {
			a: "2",
			c: 4,
			d: true,
			obj: {
				subObj: {
					f: "1",
					s: "8",
				},
			},
		},
	},
	{
		name: "X",
		body: {
			a: "2",
			c: 4,
			d: true,
			obj: {
				subObj: {
					f: "1",
					s: "8",
				},
			},
		},
	},
];

const OBJECT_CLASSES_1: ORASS.ObjectClass[] = <ORASS.ObjectClass[]><unknown>[
	{
		key: "object-class-999",
		type: NodeType.ObjectClass,
		name: "S",
		attrs: [],
		objectClasses: [],
		instances: [{
			name: "S",
			body: {
				a: {
					attr1: "abc",
				},
			},
		}],
	},
	{
		key: "object-class-123",
		type: NodeType.ObjectClass,
		name: "A",
		attrs: [{
			key: "attribute-345",
			name: "attr1",
			type: NodeType.Attribute,
			valueType: ValueType.string,
			required: true,
			examples: ["abc"],
		}],
		objectClasses: [],
		instances: [{
			name: "A",
			body: {
				attr1: "abc",
			},
		}],
	},
];

const EMPTY_DATA = {
	a: {},
	b: [
		{
			c: {},
			d: [
				{},
				{},
			],
		},
		{},
		{},
		[],
		[],
	],
};


describe("NodeHandlingService", () => {
	let service: NodeHandlingService;
	let serializer: SerializingService;

	beforeEach(async () => {
		const module: TestingModule = await Test.createTestingModule({
			providers: [
				NodeHandlingService,
				SerializingService,
			],
		}).compile();

		service = await module.get<NodeHandlingService>(NodeHandlingService);
		serializer = await module.get<SerializingService>(SerializingService);
	});

	it("must be defined", () => {
		expect(service).toBeDefined();
	});

	describe("identifyObjects(...)", () => {
		it("must identify all objects", () => {
			const foundObjects = service.identifyObjects(DATA_1);
			// console.log("FOUND OBJECTS:", inspect(foundObjects, true, 4, true));
			expect(foundObjects).toHaveLength(6);
		});
	});

	describe("identifyAllObjectClasses(...)", () => {

		let allObjectClasses;

		beforeAll(() => {
			allObjectClasses = service.identifyAllObjectClasses(OBJECTS_1);
		});

		it("must return an array of the same length as the given array of object classes", () => {
			expect(allObjectClasses).toHaveLength(OBJECTS_1.length);
		});

		it("must contain required properties", () => {
			allObjectClasses.forEach((oClass, index) => {
				expect(oClass).toHaveProperty("key");
				expect(oClass).toHaveProperty("type", NodeType.ObjectClass);
				expect(oClass).toHaveProperty("name", OBJECTS_1[index].name);
				expect(oClass).toHaveProperty("instances", [OBJECTS_1[index]]);
				expect(oClass.attrs).toBeDefined();
				expect(oClass.attrs).toBeInstanceOf(Object);
				expect(oClass.objectClasses).toBeDefined();
				expect(oClass.objectClasses).toBeInstanceOf(Array);
			});
		});
	});

	describe("identifyReferredObjectClasses(...)", () => {

		let referredObjectClasses;

		beforeAll(() => {
			referredObjectClasses = service.identifyReferredObjectClasses(OBJECT_CLASSES_1[0], OBJECT_CLASSES_1);
		});

		it("must find one referred object class", () => {
			expect(referredObjectClasses).toEqual([OBJECT_CLASSES_1[1]]);
		});
	});

	describe("nullify(...)", () => {
		it("must turn [{}, [], null, undefined] to null", () => {
			expect(service.nullify(EMPTY_DATA)).toEqual({
				a: null,
				b: [
					{
						c: null,
						d: null,
					},
				],
			});
		});
	});

	it("pascalize(...)", () => {
		expect(service.pascalize("abC xYz")).toEqual("AbcXyz");
		expect(service.pascalize("abc - xyz")).toEqual("AbcXyz");
		expect(service.pascalize("123 abc")).toEqual("123Abc");
		expect(service.pascalize("abc- _ -123")).toEqual("Abc123");
	});
});
