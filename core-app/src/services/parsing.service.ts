import { Injectable } from "@nestjs/common";
import { get } from "https";
import { parseChunked, stringifyStream } from "@discoveryjs/json-ext";
import { Observable, Subscriber, throwError } from "rxjs";
import { IncomingMessage } from "http";
import { catchError, switchMap, toArray } from "rxjs/operators";
import ReadableStream = NodeJS.ReadableStream;

@Injectable()
export class ParsingService {

	private readonly CHUNK_SIZE = 1_000_000;

	async extractData(buffers: (Buffer | string)[]): Promise<unknown[] | never> {

		const parsedDataset = [];

		for (let data of buffers) {
			let parsedData;
			if (data.length < this.CHUNK_SIZE) {
				const maybeUrl = data.toString("utf-8");
				if (this.isUrl(maybeUrl)) {
					const parsedData = await this.parseStream(this.load(maybeUrl));
					parsedDataset.push(parsedData);
					continue;
				}
			}
			parsedData = await this.parse(data);
			parsedDataset.push(parsedData);
		}

		return parsedDataset;
	}

	async parse(buffer: string | Buffer, chunkSize: number = this.CHUNK_SIZE): Promise<unknown> | never {
		let parsedData;
		try {
			parsedData = await parseChunked(function* () {
				for (let i = 0; i < buffer.length; i += chunkSize) {
					yield buffer.slice(i, Math.min(i + chunkSize, buffer.length));
					console.log(`chunk parsed (${i} - ${Math.min(i + chunkSize, buffer.length)})`);
				}
			});
		} catch (e) {
			console.error(e);
			throw new Error("Error during Buffer converting to JSON. " + e.message);
		}

		return parsedData;
	}

	async parseStream(buffer$: Observable<Buffer | string>): Promise<unknown> | never {
		return buffer$
			.pipe(
				toArray(),
				switchMap((chunks) => parseChunked(function* () {
					for (let chunk of chunks) {
						yield chunk;
						console.log(`chunk of length ${chunk.length} parsed`);
					}
				})),
				catchError((error) => {
					console.error(error);
					throw error;
				}),
			)
			.toPromise();
	}

	async stringify(data: any): Promise<string | never> {
		let chunks: string[] = [];
		return new Promise((resolve, reject) => {
			(<ReadableStream>stringifyStream(data))
				.on("data", chunk => chunks.push(chunk))
				.on("error", error => reject(error))
				.on("end", () => resolve(chunks.join("")));
		});
	}

	stringifyStream(data: any): Observable<string> {
		return new Observable((observer: Subscriber<string>) => {
			(<ReadableStream>stringifyStream(data))
				.on("data", observer.next.bind(observer))
				.on("error", throwError)
				.on("end", observer.complete.bind(observer));
		});
	}

	load(url: string): Observable<Buffer> {
		return new Observable((observer: Subscriber<Buffer>) => {
			get(url, (resp: IncomingMessage) => {
				resp.on("data", observer.next.bind(observer))
					.on("end", observer.complete.bind(observer))
					.on("error", throwError);
			});
		});
	}

	private isUrl(data: unknown): boolean {
		return typeof data === "string" && /^https?:\/\/.+\.[^"]+/.test(data);
	}
}
