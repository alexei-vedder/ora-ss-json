import {
	ConnectedSocket,
	MessageBody,
	OnGatewayConnection,
	OnGatewayDisconnect,
	SubscribeMessage,
	WebSocketGateway,
	WsResponse,
} from "@nestjs/websockets";
import { Socket } from "socket.io";
import { Observable, Subject } from "rxjs";
import { first, map } from "rxjs/operators";
import {
	ExtraInput,
	Notification,
	ProcessingProgress,
	SchemaSpawned,
	WebSocketEvent,
} from "../../shared-models/web-socket-events.model";
import { AppHelper } from "./app.helper";
import { Worker } from "worker_threads";
import { WorkerMessage, WorkerMessageEvent } from "./models/worker-message.model";


@WebSocketGateway({
	pingInterval: 20_000,
	pingTimeout: 60_000,
})
export class AppGateway implements OnGatewayConnection, OnGatewayDisconnect {

	/* WebSocket subjects */

	private readonly extraInputs$: Map<Socket, Subject<ExtraInput.RequestBody>> =
		new Map<Socket, Subject<ExtraInput.RequestBody>>();

	private readonly disconnections$: Map<Socket, Subject<void>> =
		new Map<Socket, Subject<void>>();

	/* Worker subjects */

	private readonly threads: Map<Socket, Worker> = new Map<Socket, Worker>();

	private readonly schemaSpawned$: Map<Socket, Subject<SchemaSpawned.ResponseBody>> =
		new Map<Socket, Subject<SchemaSpawned.ResponseBody>>();

	constructor(private helper: AppHelper) {
	}

	handleConnection(client: Socket, ...args: any[]): void {
		console.log("Client is connected");
		const thread = new Worker("./dist/core-app/src/worker.js");
		thread.on("message", this.handleEventFromWorker(client));
		this.threads.set(client, thread);
		this.disconnections$.set(client, new Subject());
		this.extraInputs$.set(client, new Subject());
		this.schemaSpawned$.set(client, new Subject());
	}

	handleDisconnect(client: Socket): void {
		console.log("Client is disconnected");
		this.disconnections$.get(client).next();
		this.disconnections$.get(client).complete();
		this.disconnections$.delete(client);
		this.threads.delete(client);
		this.extraInputs$.delete(client);
		this.schemaSpawned$.delete(client);
	}

	@SubscribeMessage(WebSocketEvent.DataUpload)
	dataUpload(@MessageBody() buffers: Buffer[],
			   @ConnectedSocket() client: Socket): Observable<WsResponse<SchemaSpawned.ResponseBody>> {
		console.log("data is here");

		const thread: Worker = this.threads.get(client);

		this.reportProgress(client, { task: "Extracting data" });

		thread.postMessage(<WorkerMessage>{
			event: WorkerMessageEvent.SpawnSchema,
			data: buffers,
		});

		return this.schemaSpawned$.get(client)
			.pipe(map((data) => ({
				event: WebSocketEvent.SchemaSpawned,
				data,
			})));
	}

	@SubscribeMessage(WebSocketEvent.ExtraInput)
	extraInput(@MessageBody() data: ExtraInput.RequestBody,
			   @ConnectedSocket() client: Socket): void {
		console.log("EXTRA INPUT EVENT. Data:", data);
		this.extraInputs$.get(client).next(data);
	}

	@SubscribeMessage(WebSocketEvent.GetSchema)
	getSchema(@MessageBody() { schemaId },
			  @ConnectedSocket() client: Socket): Observable<WsResponse<string>> {
		return this.helper.getSchema(schemaId);
	}

	@SubscribeMessage(WebSocketEvent.GetSchemasIds)
	getSchemasIds(@ConnectedSocket() client: Socket): Observable<WsResponse<string[]>> {
		return this.helper.getSchemasIds();
	}

	@SubscribeMessage(WebSocketEvent.GetDataInstance)
	getDataInstance(@ConnectedSocket() client: Socket,
					@MessageBody() { dataId }) {
		return this.helper.getDataInstance(dataId);
	}

	@SubscribeMessage(WebSocketEvent.GetDataInstancesIds)
	getDataInstancesIds(@ConnectedSocket() client: Socket) {
		return this.helper.getDataInstancesIds();
	}

	@SubscribeMessage(WebSocketEvent.DeleteDataInstance)
	deleteDataInstance(@ConnectedSocket() client: Socket,
					   @MessageBody() { dataId }) {
		return this.helper.deleteDataInstance(dataId);
	}

	@SubscribeMessage(WebSocketEvent.DeleteSchema)
	deleteSchema(@ConnectedSocket() client: Socket,
				 @MessageBody() { schemaId }) {
		return this.helper.deleteSchema(schemaId);
	}

	private handleEventFromWorker(client) {
		return async ({ event, data }: WorkerMessage) => {
			console.log("Message from Worker:", event);
			switch (event) {
				case WorkerMessageEvent.SpawnSchema: {
					await this.onSpawnSchemaEvent(client, data.data, data.schema);
					break;
				}

				case WorkerMessageEvent.ExtraInput: {
					this.threads.get(client).postMessage({
						event: WorkerMessageEvent.ExtraInput,
						data: await this.requestExtraInput(client, data),
					});
					break;
				}

				case WorkerMessageEvent.Notification: {
					this.sendNotification(client, data);
					break;
				}

				case WorkerMessageEvent.ProcessingProgress: {
					this.reportProgress(client, data);
					break;
				}

				case WorkerMessageEvent.ValidateData: {
					// TODO
					break;
				}
			}
		};
	}

	private async onSpawnSchemaEvent(client, data, schema) {
		if (!schema.value.nodeDataArray.length) {
			this.sendNotification(client, {
				message: "Data instance and schema wasn't saved. No entities spawned.",
				type: Notification.Type.Error,
			});
			data = schema = null;
			return;
		}

		this.reportProgress(client, { task: "Saving to DB" });

		try {
			await this.helper.saveToDB(data, schema);
			this.schemaSpawned$.get(client).next({
				schemaId: schema.id,
				dataId: data.id,
			});
		} catch ({ message }) {
			this.sendNotification(client, {
				message,
				type: Notification.Type.Error,
			});
		}
	}

	private requestExtraInput(client: Socket, responseBody: ExtraInput.ResponseBody) {
		client.emit(WebSocketEvent.ExtraInput, responseBody);
		return this.extraInputs$
			.get(client)
			.pipe(first())
			.toPromise();
	}

	private sendNotification(client: Socket, responseBody: Notification.ResponseBody) {
		client.emit(WebSocketEvent.Notification, responseBody);
	}

	private reportProgress(client: Socket, responseBody: ProcessingProgress.ResponseBody) {
		client.emit(WebSocketEvent.ProcessingProgress, responseBody);
	}
}
