import { Injectable } from "@nestjs/common";
import { ORASchemaEntityMongoDBRepository } from "./repositories/ora-schema-entity.mongodb.repository";
import { DataInstanceMongoDBRepository } from "./repositories/data-instance.mongodb.repository";
import { fromPromise } from "rxjs/internal-compatibility";
import { map, switchMap } from "rxjs/operators";
import { WebSocketEvent } from "../../shared-models/web-socket-events.model";
import { Observable } from "rxjs";
import { WsResponse } from "@nestjs/websockets";
import { ParsingService } from "./services/parsing.service";
import { DataInstance, ORASchemaEntity } from "./models/repository-entity.model";


@Injectable()
export class AppHelper {

	/**
	 * To change repository into json.repository, replace injected repositories with the following ones:
	 *
	 * @Inject(RepositoryType.DataInstanceRepository) private dataInstanceRepository: Repository<DataInstance>,
	 * @Inject(RepositoryType.SchemaRepository) private schemaRepository: Repository<ORASchemaEntity>,
	 */
	constructor(private schemaRepository: ORASchemaEntityMongoDBRepository,
				private dataInstanceRepository: DataInstanceMongoDBRepository,
				private parser: ParsingService) {
	}

	getSchema(schemaId: string): Observable<WsResponse<string>> {
		return fromPromise(this.schemaRepository.retrieve(schemaId))
			.pipe(
				switchMap((schema: ORASchemaEntity) => fromPromise(this.parser.stringify(schema))),
				map((data: string) => ({
					event: WebSocketEvent.GetSchema,
					data,
				})),
			);
	}

	getSchemasIds(): Observable<WsResponse<string[]>> {
		return fromPromise(this.schemaRepository.retrieveIds())
			.pipe(map(ids => ({
				event: WebSocketEvent.GetSchemasIds,
				data: ids,
			})));
	}

	getDataInstance(dataId: string): Observable<WsResponse<string>> {
		return fromPromise(this.dataInstanceRepository.retrieve(dataId))
			.pipe(
				switchMap((data: DataInstance) => fromPromise(this.parser.stringify(data))),
				map((data: string) => ({
					event: WebSocketEvent.GetDataInstance,
					data,
				})),
			);
	}

	getDataInstancesIds(): Observable<WsResponse<string[]>> {
		return fromPromise(this.dataInstanceRepository.retrieveIds())
			.pipe(map(ids => ({
				event: WebSocketEvent.GetDataInstancesIds,
				data: ids,
			})));
	}

	deleteSchema(schemaId): Observable<WsResponse<boolean>> {
		return fromPromise(this.schemaRepository.remove(schemaId))
			.pipe(map(success => ({
				event: WebSocketEvent.DeleteSchema,
				data: success,
			})));
	}

	deleteDataInstance(dataId): Observable<WsResponse<boolean>> {
		return fromPromise(this.dataInstanceRepository.remove(dataId))
			.pipe(map(success => ({
				event: WebSocketEvent.DeleteDataInstance,
				data: success,
			})));
	}

	async saveToDB(data: DataInstance, schema: ORASchemaEntity): Promise<void | never> {
		try {
			await this.schemaRepository.put({
				id: schema.id,
				value: schema.value,
			});

			console.log("Schema saved to DB");
		} catch (e) {
			console.error(e);
			throw e;
		}

		try {
			await this.dataInstanceRepository.put({
				id: data.id,
				value: data.value,
				schemaId: schema.id,
			});

			console.log("Data instance saved to DB");
		} catch (e) {
			console.error(e);
			throw e;
		}
	}
}
