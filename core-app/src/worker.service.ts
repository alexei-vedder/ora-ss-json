import { Injectable, OnModuleInit } from "@nestjs/common";
import { parentPort } from "worker_threads";
import { WorkerMessage, WorkerMessageEvent } from "./models/worker-message.model";
import { ParsingService } from "./services/parsing.service";
import { SchemaSpawnService } from "./services/schema-spawn.service";
import { first } from "rxjs/operators";
import { ExtraInput, Notification, ProcessingProgress } from "../../shared-models/web-socket-events.model";
import { Subject } from "rxjs";
import { v4 as uuidv4 } from "uuid";
import { DataInstance, ORASchemaEntity } from "./models/repository-entity.model";
import { SerializingService } from "./services/serializing.service";
import { ORASS } from "./models/ora-ss.model";


@Injectable()
export class WorkerService implements OnModuleInit {

	private extraInput$: Subject<ExtraInput.RequestBody> = new Subject<ExtraInput.RequestBody>();

	constructor(private parser: ParsingService,
				private schemaSpawner: SchemaSpawnService,
				private serializer: SerializingService) {
	}

	onModuleInit() {
		console.log("Worker is initialized");

		// TODO why the fuck parentPort.emit("custom-event", ...) doesn't work (from here, from App it works)?

		parentPort.on("message", ({ data, event }: WorkerMessage) => {
			console.log("Message from App:", event);
			switch (event) {
				case WorkerMessageEvent.SpawnSchema: {
					/*
					converting back to Buffer because during transferring Buffer converts to Uint8Array
					https://nodejs.org/api/worker_threads.html#worker_threads_considerations_when_transferring_typedarrays_and_buffers
					*/
					this.spawnSchema(data.map(item => Buffer.from(item)));
					break;
				}
				case WorkerMessageEvent.ExtraInput: {
					this.extraInput$.next(data);
					break;
				}
				case WorkerMessageEvent.ValidateData: {
					// TODO
					break;
				}
			}
		});

		this.extraInput$.subscribe(input => console.log("ExtraInput:", input));
	}

	private async spawnSchema(buffers: Buffer[]) {
		let data;

		try {
			data = await this.parser.extractData(buffers);
		} catch (e) {
			this.sendNotification({
				message: e.message,
				type: Notification.Type.Error,
			});
			throw e;
		}

		let schema: ORASS.Schema = await this.schemaSpawner.spawnSchema(data, {
			requestExtraInput: this.requestExtraInput.bind(this),
			sendNotification: this.sendNotification.bind(this),
			reportProgress: this.reportProgress.bind(this),
		});

		schema = await this.parser.parse(this.serializer.serialize(schema)) as ORASS.Schema;

		this.onSchemaSpawnFinish(data, schema);
	}

	private onSchemaSpawnFinish(data, schema) {
		const dataId = uuidv4();
		const schemaId = uuidv4();
		parentPort.postMessage({
			event: WorkerMessageEvent.SpawnSchema,
			data: {
				data: <DataInstance>{
					id: dataId,
					value: data,
					schemaId: schemaId,
				},
				schema: <ORASchemaEntity>{
					id: schemaId,
					value: schema,
				},
			},
		});
	}

	private requestExtraInput(resp: ExtraInput.ResponseBody) {
		parentPort.postMessage({
			event: WorkerMessageEvent.ExtraInput,
			data: resp,
		});
		return this.extraInput$
			.pipe(first())
			.toPromise();
	}

	private sendNotification(resp: Notification.ResponseBody) {
		parentPort.postMessage({
			event: WorkerMessageEvent.Notification,
			data: resp,
		});
	}

	private reportProgress(resp: ProcessingProgress.ResponseBody) {
		parentPort.postMessage({
			event: WorkerMessageEvent.ProcessingProgress,
			data: resp,
		});
	}
}
