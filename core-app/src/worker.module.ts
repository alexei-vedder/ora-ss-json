import { Module } from "@nestjs/common";
import { SchemaSpawnService } from "./services/schema-spawn.service";
import { NodeHandlingService } from "./services/node-handling.service";
import { SerializingService } from "./services/serializing.service";
import { ParsingService } from "./services/parsing.service";
import { WorkerService } from "./worker.service";


@Module({
	providers: [
		WorkerService,
		SchemaSpawnService,
		NodeHandlingService,
		SerializingService,
		ParsingService,
	],
})
export class WorkerModule {
}
