import { Module } from "@nestjs/common";
import { AppGateway } from "./app.gateway";
import { JSONRepository } from "./repositories/json.repository";
import { Repository } from "./repositories/repository";
import { RepositoryType } from "./models/repository-type.enum";
import { SchemaSpawnService } from "./services/schema-spawn.service";
import { NodeHandlingService } from "./services/node-handling.service";
import {
	Chunk,
	ChunkSchema,
	DataInstance,
	DataInstanceSchema,
	ORASchemaEntity,
	ORASchemaEntitySchema,
} from "./models/repository-entity.model";
import { SerializingService } from "./services/serializing.service";
import { MongooseModule } from "@nestjs/mongoose";
import { DataInstanceMongoDBRepository } from "./repositories/data-instance.mongodb.repository";
import { ORASchemaEntityMongoDBRepository } from "./repositories/ora-schema-entity.mongodb.repository";
import { AppHelper } from "./app.helper";
import { ParsingService } from "./services/parsing.service";
import { ChunkMongoDBRepository } from "./repositories/chunk.mongodb.repository";

const {
	MONGO_USERNAME,
	MONGO_PASSWORD,
	MONGO_HOSTNAME,
	MONGO_PORT,
	MONGO_DB,
} = process.env;

export const databaseUri = MONGO_USERNAME && MONGO_PASSWORD ?
	`mongodb://${MONGO_USERNAME}:${MONGO_PASSWORD}@${MONGO_HOSTNAME || "localhost"}:${MONGO_PORT || 27017}/${MONGO_DB || "ora-ss-json"}`
	: `mongodb://${MONGO_HOSTNAME || "localhost"}:${MONGO_PORT || 27017}/${MONGO_DB || "ora-ss-json"}`;

const schemaRepositoryService: Repository<ORASchemaEntity> =
	new JSONRepository<ORASchemaEntity>("./json-repositories/ora-schema.repository.json");
const dataInstanceRepositoryService: Repository<DataInstance> =
	new JSONRepository<DataInstance>("./json-repositories/data-instance.repository.json");

@Module({
	imports: [
		MongooseModule.forRoot(databaseUri, {
			useNewUrlParser: true,
			reconnectInterval: 500,
			connectTimeoutMS: 10000,
		}),
		MongooseModule.forFeature([
			{ name: DataInstance.name, schema: DataInstanceSchema },
			{ name: ORASchemaEntity.name, schema: ORASchemaEntitySchema },
			{ name: Chunk.name, schema: ChunkSchema },
		]),
	],
	providers: [
		AppGateway,
		AppHelper,
		SchemaSpawnService,
		NodeHandlingService,
		SerializingService,
		DataInstanceMongoDBRepository,
		ORASchemaEntityMongoDBRepository,
		ChunkMongoDBRepository,
		{
			provide: RepositoryType.SchemaRepository,
			useValue: schemaRepositoryService,
		},
		{
			provide: RepositoryType.DataInstanceRepository,
			useValue: dataInstanceRepositoryService,
		},
		ParsingService,
	],
})
export class AppModule {
}
