export const transpose = (m: any[][]) => m.length === 0 ? [] : m[0].map((x, i) => m.map(x => x[i]));
