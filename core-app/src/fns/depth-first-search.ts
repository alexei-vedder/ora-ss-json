function findNeighbors(nodeKey: string, linkDataArray: { from: string, to: string }[]): string[] {
	return linkDataArray
		.filter(link => link.from === nodeKey)
		.map(link => link.to);
}

function depthFirstSearchRecursive(linkDataArray: { from: string, to: string }[],
								   currentNodeKey: string,
								   previousNodeKey: string,
								   enterNode: (currentNodeKey: string, previousNodeKey: string) => void,
								   leaveNode: (currentNodeKey: string, previousNodeKey: string) => void): void {

	enterNode(currentNodeKey, previousNodeKey);

	findNeighbors(currentNodeKey, linkDataArray)
		.forEach(nextNode => depthFirstSearchRecursive(linkDataArray, nextNode, currentNodeKey, enterNode, leaveNode));

	leaveNode(currentNodeKey, previousNodeKey);
}

function findRootNodeKeys(linkDataArray: { from: string, to: string }[]): string[] {
	const keysFrom = [...new Set(linkDataArray.map(link => link.from))];
	const keysTo = [...new Set(linkDataArray.map(link => link.to))];
	return keysFrom
		.filter(key => !keysTo.includes(key));
}

export function depthFirstSearch(linkDataArray: { from: string, to: string }[],
								 enterNode: (currentNodeKey: string, previousNodeKey: string) => void,
								 leaveNode: (currentNodeKey: string, previousNodeKey: string) => void): void {
	findRootNodeKeys(linkDataArray).forEach(rootNodeKey => {
		depthFirstSearchRecursive(linkDataArray, rootNodeKey, null, enterNode, leaveNode);
	});
}
