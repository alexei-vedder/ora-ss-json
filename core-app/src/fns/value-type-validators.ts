import { ValueType } from "../models/type.enum";

export function isSingleValued(type: ValueType): type is ValueType.string | ValueType.number | ValueType.boolean {
	return type === ValueType.string || type === ValueType.number || type === ValueType.boolean;
}

export function isMultiValued(type: ValueType): type is ValueType.array {
	return type === ValueType.array;
}

export function isObject(type: ValueType): type is ValueType.object {
	return type === ValueType.object;
}

export function isAny(type: ValueType): type is ValueType.any {
	return type === ValueType.any;
}

export function getType(value: any): ValueType {
	switch (typeof value) {
		case ValueType.string:
		case ValueType.number:
		case ValueType.boolean:
			// @ts-ignore
			return typeof value;
		case ValueType.object:
			return Array.isArray(value) ? ValueType.array : ValueType.object;
		default:
			return ValueType.null;
	}
}
