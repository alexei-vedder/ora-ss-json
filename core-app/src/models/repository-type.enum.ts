export enum RepositoryType {
	SchemaRepository = "SchemaRepository",
	DataInstanceRepository = "DataInstanceRepository"
}
