import { GraphLinksModel } from "gojs";
import { NodeType, ValueType } from "./type.enum";
import { v4 as uuidv4 } from "uuid";
import { isAny, isMultiValued, isSingleValued } from "../fns/value-type-validators";

export namespace ORASS {

	export const ARRAY_ITEM_POSTFIX = " Item";
	export const ROOT_OBJECT_NAME = "Root$";

	export type Schema = GraphLinksModel;

	/**
	 * supports GoJS Node containing "key"
	 */
	export abstract class Node {
		/**
		 * Needs for identifying attributes with the same name
		 */
		readonly key: string;
		readonly type: NodeType;
		readonly name: string;

		protected constructor(name: string, type: NodeType) {
			this.name = name;
			this.type = type;
			this.key = this.generateNodeKey();
		}

		/**
		 * TODO SS-54
		 * Restores only plain properties!
		 * If you want to restore links to other Node instances, then
		 * after calling `fromJSON(...)` call also `Node.prototype.restoreLinks(...)`
		 */
		static fromJSON<T extends Node>(serialized: string): T {
			return Object.assign(Object.create(this.prototype), JSON.parse(serialized));
		}

		// TODO SS-54
		abstract toJSON(): string;

		// TODO SS-54
		abstract restoreLinks(nodes: Node[], linkDataArray: { from: string, to: string }[]): void;

		private generateNodeKey(): string {
			return this.type + "-" +
				this.name + "-" +
				uuidv4().slice(0, 8);
		}
	}

	export class Attribute extends Node {
		// TODO In case of composite attribute here is to add "Type.object"
		valueType: (ValueType.string | ValueType.number | ValueType.boolean) | ValueType.array | ValueType.any;
		examples: any[];
		required: boolean;
		isId: boolean;
		/**
		 * TODO SS-54
		 * Don't set directly! Use idref setter instead
		 */
		isIdref: boolean = false;
		/**
		 * TODO SS-54
		 * Don't set directly! Use idref setter instead
		 */
		target: ObjectClass;

		nonObjectClassAttr: boolean;

		constructor(name: string, valueType: ValueType, examples: any[] = [], required: boolean = true, isId: boolean = false) {
			super(name, NodeType.Attribute);

			if (isSingleValued(valueType) || isMultiValued(valueType) || isAny(valueType)) {
				this.valueType = valueType;
			} else {
				throw new Error(`Unable to create an attribute instance of ${valueType} type`);
			}

			this.examples = examples;
			this.required = required;
			this.isId = isId;
		}

		set idref(target: ObjectClass) {
			if (this.target) {
				throw new Error("Attempt to overwrite existing IDREF. Check the program logic is correct");
			}

			if (!(target instanceof ObjectClass)) {
				throw new Error("Attempt to assign to idref an object of type " + typeof target);
			}

			this.isIdref = true;
			this.target = target;
		}

		toJSON(): string {
			return JSON.stringify({
				key: this.key,
				type: this.type,
				name: this.name,
				valueType: this.valueType,
				examples: this.examples?.slice(0, Math.min(this.examples.length, 5)),
				required: this.required,
				isId: this.isId,
				isIdref: this.isIdref,
				nonObjectClassAttr: this.nonObjectClassAttr,
			});
		}

		restoreLinks(nodes: ORASS.Node[], linkDataArray: { from: string; to: string }[]): void {
			const linksToRestore = linkDataArray.filter(link => link.from === this.key);
			linksToRestore.forEach(link => {
				const target = nodes.find(node => node.key === link.to);
				if (target instanceof ORASS.ObjectClass) {
					this.idref = target;
				}
			});
		}
	}

	export class ObjectClass extends Node {

		attrs: Attribute[];
		objectClasses: ObjectClass[];
		instances: Object[];

		constructor(name: string, instances: Object[] = [], attrs: Attribute[] = [], objectClasses: ObjectClass[] = []) {
			super(name, NodeType.ObjectClass);
			this.instances = instances;
			this.attrs = attrs;
			this.objectClasses = objectClasses;
		}

		get singleAttrs(): Attribute[] {
			return this.attrs.filter(attr => isSingleValued(attr.valueType));
		}

		get multiAttrs(): Attribute[] {
			return this.attrs.filter(attr => isMultiValued(attr.valueType));
		}

		/**
		 * identifier.length > 1 stands for composite identifier
		 */
		get identifier(): Attribute[] {
			return this.attrs.filter(attr => attr.isId);
		}

		get idrefAttrs(): Attribute[] {
			return this.attrs.filter(attr => attr.isIdref);
		}

		get nonObjectClassAttrs(): Attribute[] {
			return this.attrs.filter(attr => attr.nonObjectClassAttr);
		}

		toJSON(): string {
			return JSON.stringify({
				key: this.key,
				type: this.type,
				name: this.name,
				// instances: this.instances, // TODO how to calculate a size of an object? only small instances must pass if any
			});
		}

		restoreLinks(nodes: ORASS.Node[], linkDataArray: { from: string; to: string }[]): void {
			const linksToRestore = linkDataArray.filter(link => link.from === this.key);
			this.objectClasses = [];
			this.attrs = [];
			linksToRestore.forEach(link => {
				const target = nodes.find(node => node.key === link.to);
				if (target instanceof ORASS.ObjectClass) {
					this.objectClasses.push(target);
				} else if (target instanceof ORASS.Attribute) {
					this.attrs.push(target);
				}
			});
		}
	}

	export interface Object {
		name: string;

		/**
		 * 	mustn't be empty
		 */
		body: ObjectBody
	}

	export interface ObjectBody {
		[key: string]: unknown;
	}

	export class RelationshipTypeTable {

		leafToRootPath: string[];
		head: Attribute[] = [];
		body: any[][] = [];
		/**
		 * range has a form of [firstIndexIncluded, lastIndexExcluded]
		 */
		private domain: {
			nonOCAttrsRange: [number, number];
			leafIdRange: [number, number];
			parentIdRanges: [number, number][];
		};

		constructor(path: string[], leafIdentifier: Attribute[], nonOCAttrs: Attribute[] = []) {
			this.leafToRootPath = path;
			const nonOCAttrsLength = this.head.push(...nonOCAttrs);
			const totalLength = this.head.push(...leafIdentifier);
			this.domain = {
				nonOCAttrsRange: [0, nonOCAttrsLength],
				leafIdRange: [nonOCAttrsLength, totalLength],
				parentIdRanges: [],
			};
		}

		get nonOCAttrs(): Attribute[] {
			const range = this.domain.nonOCAttrsRange;
			return this.head.slice(range[0], range[1]);
		}

		get leafId(): Attribute[] {
			const range = this.domain.leafIdRange;
			return this.head.slice(range[0], range[1]);
		}

		get nonOCAttrsColumns(): any[][] {
			const range = this.domain.nonOCAttrsRange;
			return this.body.slice(range[0], range[1]);
		}

		get leafColumns(): any[][] {
			const range = this.domain.leafIdRange;
			return this.body.slice(range[0], range[1]);
		}

		setParentId(attrs: Attribute[]): void {
			if (attrs.some(attr => !attr.isId)) {
				throw new Error("Attempt to assign non-id attributes as id");
			}
			const lengthBefore = this.head.length;
			const lengthAfter = this.head.push(...attrs);
			this.domain.parentIdRanges.push([lengthBefore, lengthAfter]);
		}

		setColumn(attrKey: string, column: any[]): void {
			const columnIndex = this.head.findIndex(attr => attr.key === attrKey);
			this.body[columnIndex] = column;
		}

		getParentColumns(index: number): any[][] {
			const range = this.domain.parentIdRanges[index];
			return this.body.slice(range[0], range[1]);
		}

		getObjectClassKey(attrKey: string): string {
			const columnIndex = this.head.findIndex(attr => attr.key === attrKey);
			if (this.domain.nonOCAttrsRange[0] <= columnIndex && columnIndex < this.domain.leafIdRange[1]) {
				return this.leafToRootPath[0];
			} else {
				const parentIndex = this.domain.parentIdRanges
					.findIndex(range => range[0] <= columnIndex && columnIndex < range[1]);
				return this.leafToRootPath[parentIndex + 1];
			}
		}

		getParentIdentifier(index: number): Attribute[] {
			const range = this.domain.parentIdRanges[index];
			return this.head.slice(range[0], range[1]);
		}
	}

	export interface RelationshipType {
		name: string; // smth like CoursesItem-StudentsItem-Tutor
		path: string[]; // ObjectClasses keys
		degree: number;
		parentParticipationConstraint: [number, number];
		childParticipationConstraint: [number, number];
		attribute: Attribute;
	}
}
