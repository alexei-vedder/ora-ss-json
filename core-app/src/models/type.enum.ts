export enum ValueType {
	string = "string",
	number = "number",
	boolean = "boolean",
	object = "object",
	array = "array",
	any = "any",
	null = "null"
}

export enum NodeType {
	ObjectClass = "ObjectClass",
	Attribute = "Attribute"
}
