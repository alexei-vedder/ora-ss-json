import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import { Document } from "mongoose";

export class RepositoryEntity {
	id: string; // UUID
	value?: any;
}

export class ChunkedRepositoryEntity extends RepositoryEntity {
	chunkIds?: string[];
}

@Schema({ collection: "ora-schema" })
export class ORASchemaEntity extends ChunkedRepositoryEntity {
	@Prop() id: string; // UUID
	@Prop() chunkIds?: string[];
}

@Schema({ collection: "data-instance" })
export class DataInstance extends ChunkedRepositoryEntity {
	@Prop() id: string; // UUID
	@Prop() schemaId: string; // UUID
	@Prop() name?: string; // if user wants to set some name for uploaded data,
	@Prop() chunkIds?: string[];
}

@Schema({ collection: "chunk" })
export class Chunk extends RepositoryEntity {
	@Prop() id: string;
	@Prop() entityId: string;
	@Prop() value: string; // stringified part of an object
}

export const DataInstanceSchema = SchemaFactory.createForClass(DataInstance);
export const ORASchemaEntitySchema = SchemaFactory.createForClass(ORASchemaEntity);
export const ChunkSchema = SchemaFactory.createForClass(Chunk);

export type DataInstanceDocument = DataInstance & Document;
export type ORASchemaEntityDocument = ORASchemaEntity & Document;
export type ChunkDocument = Chunk & Document;
