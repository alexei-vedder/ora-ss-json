export interface WorkerMessage<T = any> {
	event: WorkerMessageEvent,
	data: T
}

export enum WorkerMessageEvent {
	SpawnSchema = "spawn-schema",
	ExtraInput = "extra-input",
	Notification = "notification",
	ProcessingProgress = "processing-progress",
	ValidateData = "validate-data",
}
