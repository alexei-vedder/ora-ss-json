# ORA-SS JSON

Visit at http://orassjson.ddns.net/

## Demo

![](./demo/demo.gif)

---

## How to deploy a new version:

* build `core-app` via `docker build -t alvedder/ora-ss-json:<version> .` in the project root directory

* push the built image to Docker Hub

* update `docker-compose.yml`

* build `client` via `npm run build`

* go to DigitalOcean droplet (WinSCP)

* replace `etc/ora-ss-json-core-app/docker-compose.yml` with `docker-compose.yml` from the project repository, and, if
  needed, `.env`
* open `/var/www/html/orassjson.ddns.net/public/frontend` and replace existing `client/dist` files with newly built ones

* go to the droplet console (PuTTY or web-terminal)

* go to `etc/ora-ss-json-core-app/` directory and run `docker-compose up -d`

* check if there are stopped containers via `docker container ls --all` and remove them
  via `docker container rm <container id>`
