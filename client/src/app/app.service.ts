import { Injectable } from "@angular/core";
import { Socket } from "ngx-socket-io";
import {
	ExtraInput,
	Notification,
	ProcessingProgress,
	SchemaSpawned,
	WebSocketEvent,
} from "../../../shared-models/web-socket-events.model";
import { ReplaySubject } from "rxjs";


@Injectable()
export class AppService {

	readonly extraInputRequest$ = this.socket.fromEvent<ExtraInput.ResponseBody>(WebSocketEvent.ExtraInput);
	readonly processingProgress$ = this.socket.fromEvent<ProcessingProgress.ResponseBody>(WebSocketEvent.ProcessingProgress);
	readonly schemaSpawned$ = this.socket.fromEvent<SchemaSpawned.ResponseBody>(WebSocketEvent.SchemaSpawned);
	readonly getSchema$ = this.socket.fromEvent<string>(WebSocketEvent.GetSchema);
	readonly getSchemasIds$ = this.socket.fromEvent<string[]>(WebSocketEvent.GetSchemasIds);
	readonly getDataInstance$ = this.socket.fromEvent<string>(WebSocketEvent.GetDataInstance);
	readonly getDataInstancesIds$ = this.socket.fromEvent<string[]>(WebSocketEvent.GetDataInstancesIds);
	readonly deleteSchema$ = this.socket.fromEvent<boolean>(WebSocketEvent.DeleteSchema);
	readonly deleteDataInstance$ = this.socket.fromEvent<boolean>(WebSocketEvent.DeleteDataInstance);
	readonly notification$ = this.socket.fromEvent<Notification.ResponseBody>(WebSocketEvent.Notification);
	readonly localNotification$ = new ReplaySubject<Notification.ResponseBody>(1);

	constructor(private socket: Socket) {

		this.socket.on("connect", () => {
			this.localNotification$.next({
				message: "Connected",
				type: Notification.Type.Info,
			});
		});

		this.socket.on("disconnect", () => {
			this.localNotification$.next({
				message: "Disconnected",
				type: Notification.Type.Error,
			});
		});

		this.socket.on("connect_error", (reason) => {
			this.localNotification$.next({
				message: "Connection error! Trying to reconnect",
				type: Notification.Type.Error,
			});
			if (reason === "io server disconnect") {
				this.socket.connect();
			}
		});
	}

	sendExtraInput(input: ExtraInput.RequestBody): void {
		this.socket.emit(WebSocketEvent.ExtraInput, input);
	}

	sendDataInstances(data: Blob[]): void {
		this.socket.emit(WebSocketEvent.DataUpload, data);
	}

	sendDownloadSchemaRequest(schemaId: string): void {
		this.socket.emit(WebSocketEvent.GetSchema, { schemaId });
	}

	sendLoadDataInstanceRequest(dataId: string): void {
		this.socket.emit(WebSocketEvent.GetDataInstance, { dataId });
	}

	sendLoadSchemasIdsRequest(): void {
		this.socket.emit(WebSocketEvent.GetSchemasIds);
	}

	sendLoadDataInstancesIdsRequest(): void {
		this.socket.emit(WebSocketEvent.GetDataInstancesIds);
	}

	sendDeleteDataInstanceRequest(dataId: string): void {
		this.socket.emit(WebSocketEvent.DeleteDataInstance, { dataId });
	}

	sendDeleteSchemaRequest(schemaId: string): void {
		this.socket.emit(WebSocketEvent.DeleteSchema, { schemaId });
	}
}
