import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";

import { AppComponent } from "./app.component";
import { SocketIoConfig, SocketIoModule } from "ngx-socket-io";
import { AppService } from "./app.service";
import { ExtraInputFormComponent } from "./components/extra-input-form/extra-input-form.component";
import { FormsModule } from "@angular/forms";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { MatCheckboxModule } from "@angular/material/checkbox";
import { MatButtonModule } from "@angular/material/button";
import { MatFormFieldModule } from "@angular/material/form-field";
import { DataUploadFormComponent } from "./components/data-upload-form/data-upload-form.component";
import { MatInputModule } from "@angular/material/input";
import { SchemaDiagramComponent } from "./components/schema-diagram/schema-diagram.component";
import { FilePondModule } from "ngx-filepond";
import { NotificationComponent } from "./components/notification/notification.component";
import { MatSnackBarModule } from "@angular/material/snack-bar";
import { MatCardModule } from "@angular/material/card";
import { MatExpansionModule } from "@angular/material/expansion";
import { MatProgressSpinnerModule } from "@angular/material/progress-spinner";
import { ParsingService } from "./parsing.service";
import { CodeBlockComponent } from "./components/code-block/code-block.component";
import { environment } from "../environments/environment";


const config: SocketIoConfig = { url: environment.backendUrl, options: {} };

@NgModule({
	declarations: [
		AppComponent,
		ExtraInputFormComponent,
		DataUploadFormComponent,
		SchemaDiagramComponent,
		NotificationComponent,
		CodeBlockComponent,
	],
	imports: [
		BrowserModule,
		SocketIoModule.forRoot(config),
		FormsModule,
		BrowserAnimationsModule,

		MatCheckboxModule,
		MatFormFieldModule,
		MatSnackBarModule,
		MatInputModule,
		MatButtonModule,
		MatCardModule,
		MatExpansionModule,
		MatProgressSpinnerModule,

		FilePondModule,
	],
	providers: [
		AppService,
		ParsingService,
	],
	bootstrap: [AppComponent],
})
export class AppModule {
}
