import { AfterViewInit, Component, OnDestroy, OnInit, ViewChild } from "@angular/core";
import { AppService } from "./app.service";
import { merge, Subject } from "rxjs";
import { first, map, switchMap, takeUntil, tap } from "rxjs/operators";
import {
	ExtraInput,
	Notification,
	ProcessingProgress,
	SchemaSpawned,
} from "../../../shared-models/web-socket-events.model";
import { Option } from "./components/extra-input-form/extra-input-form.component";
import { saveAs } from "file-saver";
import { NotificationComponent } from "./components/notification/notification.component";
import { ParsingService } from "./parsing.service";
import { animate, style, transition, trigger } from "@angular/animations";

@Component({
	selector: "ss-root",
	animations: [
		trigger(
			"appearingAnimation",
			[
				transition(
					":enter",
					[
						style({ height: 0, opacity: 0 }),
						animate("1s ease-out",
							style({ height: "100%", opacity: 1 })),
					],
				),
			],
		),
	],
	templateUrl: "app.component.html",
	styleUrls: ["app.component.less"],
})
export class AppComponent implements OnInit, AfterViewInit, OnDestroy {

	extraInput$: Subject<Option[]> = new Subject<Option[]>();

	currentOptions: Option[];
	currentMessage: string;
	currentExamples: any[];
	currentMode: ExtraInput.SelectMode = ExtraInput.SelectMode.Free;
	progress: ProcessingProgress.ResponseBody;
	loading: boolean = false;

	dataUploaded: boolean = false;
	schemaSpawned: boolean = false;

	visualizedSchema: any;
	storedSchemasIds: string[] = [];
	spawnedSchemaId: string;

	storedDataInstancesIds: string[] = [];
	private uploadedDataId: string;

	private unsubscribe: Subject<void> = new Subject<void>();

	@ViewChild("notification")
	private notification: NotificationComponent;

	constructor(private service: AppService,
				private parser: ParsingService) {
	}

	ngOnInit(): void {
		this.loadStoredSchemasIds();
		this.loadStoredDataInstancesIds();

		this.service.processingProgress$
			.pipe(takeUntil(this.unsubscribe))
			.subscribe(message => {
				this.progress = message;
			});

		this.service.extraInputRequest$
			.pipe(
				takeUntil(this.unsubscribe),
				tap((requestBody: ExtraInput.ResponseBody) => this.prepareExtraInputForm(requestBody)),
				switchMap(() => this.extraInput$),
				map((options: Option[]) => ({
					answer: options
						.filter(option => option.checked)
						.map(option => option.name),
				})),
				tap((responseBody: ExtraInput.RequestBody) => {
					this.service.sendExtraInput(responseBody);
					this.clearExtraInputForm();
				}),
			)
			.subscribe();

		this.service.schemaSpawned$
			.pipe(takeUntil(this.unsubscribe))
			.subscribe((schemaSpawnedResponse: SchemaSpawned.ResponseBody) => {
				this.schemaSpawned = true;
				this.spawnedSchemaId = schemaSpawnedResponse.schemaId;
				this.uploadedDataId = schemaSpawnedResponse.dataId;
				this.loadStoredSchemasIds();
				this.loadStoredDataInstancesIds();
			});

		this.service.deleteSchema$
			.pipe(takeUntil(this.unsubscribe))
			.subscribe(deleted => {
				this.loadStoredSchemasIds();
			});

		this.service.deleteDataInstance$
			.pipe(takeUntil(this.unsubscribe))
			.subscribe(deleted => {
				this.loadStoredDataInstancesIds();
			});
	}

	ngAfterViewInit(): void {
		merge(this.service.notification$,
			this.service.localNotification$)
			.pipe(takeUntil(this.unsubscribe))
			.subscribe((notificationResponse: Notification.ResponseBody) => {
				// TODO handle type of notification (at least different style)
				this.notification.openSnackBar(notificationResponse.message, notificationResponse.action);
				if (notificationResponse.type === Notification.Type.Error) {
					this.prepareUploadingAnotherData();
				}
			});
	}

	ngOnDestroy() {
		this.unsubscribe.next();
		this.unsubscribe.complete();
	}

	sendDataInstance(data: Blob[]): void {
		this.service.sendDataInstances(data);
		this.dataUploaded = true;
	}

	loadStoredSchemasIds(): void {
		this.service.sendLoadSchemasIdsRequest();
		this.service.getSchemasIds$
			.pipe(first())
			.subscribe((schemasIds: string[]) => {
				this.storedSchemasIds = schemasIds.reverse();
				this.loading = false;
			});
	}

	loadStoredDataInstancesIds(): void {
		this.service.sendLoadDataInstancesIdsRequest();
		this.service.getDataInstancesIds$
			.pipe(first())
			.subscribe((dataIds: string[]) => {
				this.storedDataInstancesIds = dataIds.reverse();
				this.loading = false;
			});
	}

	downloadDataInstance(dataId: string): void {
		this.service.sendLoadDataInstanceRequest(dataId);
		this.service.getDataInstance$
			.pipe(first())
			.subscribe(async (data: string) => {
				saveAs(new Blob([data]),
					`data-instance-${dataId.slice(0, 8)}.json`);
			});
	}

	downloadSchema(schemaId: string): void {
		this.service.sendDownloadSchemaRequest(schemaId);
		this.service.getSchema$
			.pipe(first())
			.subscribe(async (schema: string) => {
				const parsed: any = await this.parser.parse(schema);
				saveAs(new Blob([JSON.stringify(parsed, null, 4)]),
					`ora-ss-schema-${parsed.id.slice(0, 8)}.json`);
			});
	}

	buildSchemaGraph(schemaId: string): void {
		this.service.sendDownloadSchemaRequest(schemaId);
		this.service.getSchema$
			.pipe(first())
			.subscribe(async (schema: string) => {
				this.visualizedSchema = null;
				this.visualizedSchema = await this.parser.parse(schema);
			});
	}

	deleteSchema(schemaId: string) {
		this.service.sendDeleteSchemaRequest(schemaId);
		this.loading = true;
	}

	deleteDataInstance(dataId: string) {
		this.service.sendDeleteDataInstanceRequest(dataId);
		this.loading = true;
	}

	prepareUploadingAnotherData(): void {
		this.schemaSpawned = false;
		this.spawnedSchemaId = null;
		this.dataUploaded = false;
		this.visualizedSchema = null;
		this.currentOptions = null;
		this.progress = null;
	}

	private prepareExtraInputForm(requestBody: ExtraInput.ResponseBody): void {
		this.currentMessage = requestBody.message;
		this.currentExamples = requestBody.examples.map(ex => JSON.stringify(ex, null, 4));
		this.currentOptions = requestBody.options.map(option => ({
			name: option,
			checked: false,
		}));
		this.currentMode = requestBody.mode;
	}

	private clearExtraInputForm(): void {
		this.currentMessage = null;
		this.currentOptions = null;
		this.currentExamples = null;
		this.currentMode = ExtraInput.SelectMode.Free;
	}
}
