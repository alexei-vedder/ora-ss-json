import { Injectable } from "@angular/core";
import { parseChunked, stringifyStream } from "@discoveryjs/json-ext";

@Injectable()
export class ParsingService {

	private readonly CHUNK_SIZE = 10_000;

	async parse(buffer: string, chunkSize: number = this.CHUNK_SIZE): Promise<unknown | never> {
		let parsedData;
		try {
			parsedData = await parseChunked(function* () {
				for (let i = 0; i < buffer.length; i += chunkSize) {
					yield buffer.slice(i, Math.min(i + chunkSize, buffer.length));
					console.log(`chunk parsed (${i} - ${Math.min(i + chunkSize, buffer.length)})`);
				}
			});
		} catch (e) {
			console.error(e);
			throw new Error("Error during parsing. " + e.message);
		}

		return parsedData;
	}

	async stringify(data): Promise<string | never> {
		let chunks: string[] = [];
		return new Promise((resolve, reject) => {
			stringifyStream(data)
				.on("data", chunk => chunks.push(chunk))
				.on("error", error => reject(error))
				.on("end", () => resolve(chunks.join("")));
		});

	}
}
