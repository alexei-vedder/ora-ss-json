import { Component, Input, OnInit } from "@angular/core";
import { Diagram, GraphLinksModel, GraphObject, Margin, TreeLayout } from "gojs";
import { objectClassTemplate } from "./templates/object-class.template";
import { attributeTemplate } from "./templates/attribute.template";
import { linkTemplate } from "./templates/link.template";


@Component({
	selector: "ss-schema-diagram",
	template: `
		<div class="schema-diagram-wrapper">
			<button class="schema-diagram__save-button"
					(click)="saveAsPNG()"
					mat-stroked-button
					color="primary">
				Save as PNG
			</button>
			<div id="schema-diagram"></div>
		</div>
	`,
	styles: [`
		.schema-diagram-wrapper {
			margin-top: 40px;
		}

		.schema-diagram__save-button {
			margin-bottom: 20px;
		}

		#schema-diagram {
			background-color: white;
			border: solid 1px black;
			width: 100%;
			height: 90vh
		}
	`],
})
export class SchemaDiagramComponent implements OnInit {

	@Input()
	schema;

	private diagram;

	ngOnInit(): void {
		const $ = GraphObject.make;

		this.diagram = $(Diagram, "schema-diagram",  // name must refer to the DIV HTML element
			{
				allowDelete: false,
				allowCopy: false,
				layout: $(TreeLayout),
				"undoManager.isEnabled": true,
				padding: 20,
			},
		);

		this.diagram.nodeTemplateMap.add("ObjectClass", objectClassTemplate);

		this.diagram.nodeTemplateMap.add("Attribute", attributeTemplate);

		this.diagram.linkTemplate = linkTemplate;

		this.diagram.model = $(GraphLinksModel,
			{
				copiesArrays: true,
				copiesArrayObjects: true,
				nodeDataArray: this.schema.value.nodeDataArray,
				linkDataArray: this.schema.value.linkDataArray,
				nodeCategoryProperty: "type",
			},
		);
	}

	saveAsPNG() {
		const callback = (blob) => {
			const url = window.URL.createObjectURL(blob);
			const filename = `ora-ss-schema-${this.schema.id.slice(0, 8)}.png`;

			const a = document.createElement("a");
			a.style.display = "none";
			a.href = url;
			a.download = filename;
			document.body.appendChild(a);

			requestAnimationFrame(() => {
				a.click();
				window.URL.revokeObjectURL(url);
				document.body.removeChild(a);
			});
		};

		this.diagram.makeImageData({
			background: "white",
			returnType: "blob",
			scale: 1,
			padding: new Margin(30),
			callback,
		});
	}
}
