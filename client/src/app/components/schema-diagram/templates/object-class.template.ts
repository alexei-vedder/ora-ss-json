import {
	Binding,
	GraphObject,
	Margin,
	Node,
	Panel,
	Part,
	Point,
	RowColumnDefinition,
	Shape,
	Spot,
	TextBlock,
} from "gojs";

const $ = GraphObject.make;

export const objectClassTemplate = $(Node, "Auto",  // the whole node panel
	{
		selectionAdorned: true,
		resizable: true,
		layoutConditions: Part.LayoutStandard & ~Part.LayoutNodeSized,
		fromSpot: Spot.AllSides,
		toSpot: Spot.AllSides,
		isShadowed: true,
		shadowOffset: new Point(3, 3),
		shadowColor: "#C5C1AA",
	},
	//	new Binding("location", "location").makeTwoWay(),
	// whenever the PanelExpanderButton changes the visible property of the "LIST" panel,
	// clear out any desiredSize set by the ResizingTool.
	/*new Binding("desiredSize", "visible", (v) => new Size(NaN, NaN))
		.ofObject("LIST"),*/
	// define the node's outer shape, which will surround the Table
	$(Shape, "Rectangle",
		{ fill: "white", stroke: "#eeeeee", strokeWidth: 3 }),
	$(Panel, "Table",
		{ margin: 8, stretch: GraphObject.Fill },
		$(RowColumnDefinition, { row: 0, sizing: RowColumnDefinition.None }),
		// the table header
		$(TextBlock,
			{
				row: 0, alignment: Spot.Center,
				margin: 5,  // leave room for Button
				font: "bold 16px Consolas, sans-serif",
			},
			new Binding("text", "name")),
	),
);
