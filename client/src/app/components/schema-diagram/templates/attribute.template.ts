import { Binding, GraphObject, Margin, Node, Panel, Part, Point, Shape, Size, Spot, TextBlock } from "gojs";

const $ = GraphObject.make;

/** @deprecated */
const colors = {
	"red": "#be4b15",
	"green": "#52ce60",
	"blue": "#6ea5f8",
	"lightred": "#fd8852",
	"lightblue": "#afd4fe",
	"lightgreen": "#b9e986",
	"pink": "#faadc1",
	"purple": "#d689ff",
	"orange": "#fdb400",
};


export const attributeTemplate = $(Node, "Vertical",
	{
		selectionAdorned: true,
		resizable: false,
		layoutConditions: Part.LayoutStandard & ~Part.LayoutNodeSized,
		fromSpot: Spot.AllSides,
		toSpot: Spot.AllSides,
		isShadowed: true,
		shadowOffset: new Point(3, 3),
		shadowColor: "#C5C1AA",
	},
	/** Attribute icon */
	$(Panel, "Auto", {},
		$(Shape,
			{
				strokeWidth: 3,
				desiredSize: new Size(30, 30),
				stroke: "gray",
			},
			new Binding("fill", null, (value, obj) => {
				const attr = obj.findTemplateBinder().data;
				return attr.isId ? "gray" : "white";
			}),
			new Binding("figure", null, (value, obj) => {
				const { nonObjectClassAttr } = obj.findTemplateBinder().data;
				return nonObjectClassAttr ? "Diamond" : "Circle";
			}),
		),
		$(TextBlock,
			new Binding("font", null, (value, obj) => {
				const attr = obj.findTemplateBinder().data;
				if (attr.valueType === "any") {
					return "bold 12px Consolas, sans-serif";
				}
				return "bold 16px Consolas, sans-serif";
			}),
			new Binding("text", null, (value, obj) => {
				const attr = obj.findTemplateBinder().data;
				let text = "";
				if (!attr.required && attr.valueType === "array") {
					text = "*";
				} else if (attr.required && attr.valueType === "array") {
					text = "+";
				} else if (!attr.required && attr.valueType !== "array") {
					text = "?";
				} else if (attr.valueType === "any") {
					text = "ANY";
				}
				return text;
			}),
		),
	),
	/** Attribute name */
	$(TextBlock,
		{
			alignment: Spot.Center,
			margin: new Margin(10, 0, 0, 0),
			font: "bold 16px Consolas, sans-serif",
		},
		new Binding("text", "name"),
	),
	$("PanelExpanderButton", "PARAMS",
		{
			alignment: Spot.TopLeft,
		},
	),
	/** Other attribute information */
	$(Panel, "Vertical",
		{
			name: "PARAMS",
			padding: 3,
			alignment: Spot.TopLeft,
			defaultAlignment: Spot.Left,
			stretch: GraphObject.Fill,
			minSize: new Size(100, 100),
			visible: false,
		},
		$(TextBlock,
			{
				stroke: "#333333",
				font: "bold 14px sans-serif",
				alignment: Spot.Left,
				margin: new Margin(5, 0, 0, 0),
				isMultiline: false,
			},
			new Binding("text", "valueType", val => `Type: ${val}`),
		),
		$(TextBlock,
			{
				stroke: colors.blue,
				font: "bold 14px sans-serif",
				alignment: Spot.Left,
				margin: new Margin(5, 0, 0, 0),
				isMultiline: false,
			},
			new Binding("text", "isId", val => val ? "ID" : ""),
			new Binding("visible", "isId"),
		),
		$(TextBlock,
			{
				stroke: colors.blue,
				font: "bold 14px sans-serif",
				alignment: Spot.Left,
				margin: new Margin(5, 0, 0, 0),
				isMultiline: false,
			},
			new Binding("text", "isIdref", val => val ? "IDREF" : ""),
			new Binding("visible", "isIdref"),
		),
		$(TextBlock,
			{
				stroke: colors.blue,
				font: "bold 14px sans-serif",
				alignment: Spot.Left,
				margin: new Margin(5, 0, 0, 0),
				isMultiline: false,
			},
			new Binding("text", "nonObjectClassAttr", val => val ? "Non-o-class attr" : ""),
			new Binding("visible", "nonObjectClassAttr"),
		),
	),
);
