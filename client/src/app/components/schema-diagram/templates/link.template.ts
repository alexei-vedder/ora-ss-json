import { Binding, GraphObject, Link, Point, Shape, TextBlock } from "gojs";

const $ = GraphObject.make;

export const linkTemplate = $(Link,
	{
		selectionAdorned: true,
		layerName: "Foreground",
		reshapable: true,
		routing: Link.AvoidsNodes,
		corner: 5,
		curve: Link.JumpGap,
	},
	$(Shape,
		{
			stroke: "#303B45",
			strokeWidth: 2.5,
		},
		new Binding("strokeDashArray", null, (value, obj) => {
			const { from } = obj.findTemplateBinder().data;
			const fromNode = obj.diagram.model.nodeDataArray.find(node => node.key === from);
			return fromNode?.isIdref ? [3, 2] : [0, 0];
		}),
	),
	$(TextBlock,  // the "from" label
		{
			textAlign: "center",
			font: "bold 12px sans-serif",
			stroke: "#1967B3",
			segmentIndex: 0,
			segmentOffset: new Point(NaN, NaN),
			segmentOrientation: Link.OrientUpright,
		},
		new Binding("text", null, (value, obj) => {
			const { relationshipType } = obj.findTemplateBinder().data;
			if (!relationshipType) {
				return;
			}
			return `
			${relationshipType.name},
			degree: ${relationshipType.degree},
			parent: ${JSON.stringify(relationshipType.parentParticipationConstraint)},
			child: ${JSON.stringify(relationshipType.childParticipationConstraint)}
			`;
		}),
	),
	$(TextBlock,  // the "to" label
		{
			textAlign: "center",
			font: "bold 14px sans-serif",
			stroke: "#1967B3",
			segmentIndex: -1,
			segmentOffset: new Point(NaN, NaN),
			segmentOrientation: Link.OrientUpright,
		},
		new Binding("text", "toText"),
	),
	$(Shape,
		{
			toArrow: "StretchedDiamond",
			fill: "black",
		},
	),
);
