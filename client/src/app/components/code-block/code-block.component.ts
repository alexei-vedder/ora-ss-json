import { Component, Input } from "@angular/core";

@Component({
	selector: "ss-code-block",
	template: `
		<section *ngIf="blocks?.length" class="code-block">
			<code>
				<h3><strong>{{title}}</strong></h3>
				<span *ngFor="let block of blocks">{{block}}</span>
			</code>
		</section>
	`,
	styleUrls: ["code-block.component.less"],
})
export class CodeBlockComponent {

	@Input()
	blocks: string[];

	@Input()
	title: string;

}
