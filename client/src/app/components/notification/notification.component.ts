import { Component } from "@angular/core";
import { MatSnackBar } from "@angular/material/snack-bar";

@Component({
	selector: "ss-notification",
	template: `
	`,
	styles: [],
})
// TODO support different types (different styles for each type)
export class NotificationComponent {

	constructor(private snackBar: MatSnackBar) {
	}

	openSnackBar(message: string, action: string = "Got it") {
		this.snackBar.open(message, action);
	}
}
