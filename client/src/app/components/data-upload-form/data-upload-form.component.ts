import { Component, EventEmitter, Output } from "@angular/core";

@Component({
	selector: "ss-data-upload-form",
	template: `
		<mat-form-field appearance="fill">
			<mat-label>Paste JSON data or URL here</mat-label>
			<textarea #textArea [maxLength]="MAX_TEXT_AREA_LENGTH"
					  [(ngModel)]="uploadedTextData" matInput></textarea>
			<mat-hint align="end">{{textArea.value.length}} / {{MAX_TEXT_AREA_LENGTH}}</mat-hint>
		</mat-form-field>

		<file-pond #fileUploader
				   [options]="pondOptions"
				   (onaddfile)="onFileUpload($event)">
		</file-pond>

		<button mat-raised-button
				color="primary"
				[disabled]="!uploadedTextData && uploadedFiles.length === 0"
				(click)="sendData()">
			Upload
		</button>
	`,
	styles: [`
		mat-form-field {
			display: block;
			margin: 50px 0 20px 0;
		}

		mat-form-field textarea {
			font-family: "Consolas";
		}
	`],
})
export class DataUploadFormComponent {

	uploadedFiles: File[] = [];
	uploadedTextData: string;

	readonly pondOptions = {
		multiple: true,
		labelIdle: "Drop files here or click",
		acceptedFileTypes: "json",
	};

	readonly MAX_TEXT_AREA_LENGTH = 10_000;

	@Output()
	dataUpload: EventEmitter<Blob[]> = new EventEmitter();

	onFileUpload({ file: { file } }) {
		this.uploadedFiles.push(file);
	}

	sendData() {
		const result: Blob[] = [];
		if (this.uploadedFiles.length) {
			result.push(...this.uploadedFiles);
		}
		if (this.uploadedTextData) {
			result.push(new Blob([this.uploadedTextData]));
		}
		this.dataUpload.emit(result);
	}

}
