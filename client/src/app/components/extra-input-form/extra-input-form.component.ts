import { Component, EventEmitter, Input, Output } from "@angular/core";
import { ExtraInput } from "../../../../../shared-models/web-socket-events.model";

export interface Option {
	name: string;
	checked: boolean;
}

@Component({
	selector: "ss-extra-input-form",
	templateUrl: "extra-input-form.component.html",
	styleUrls: ["extra-input-form.component.less"],
})
export class ExtraInputFormComponent {

	@Input()
	examples: any[];

	@Input()
	options: Option[] = [];

	@Input()
	message: string = "";

	@Output()
	submit: EventEmitter<Option[]> = new EventEmitter<Option[]>();

	valid: boolean = false;

	private _mode: ExtraInput.SelectMode;

	get mode(): ExtraInput.SelectMode {
		return this._mode;
	}

	@Input()
	set mode(value: ExtraInput.SelectMode) {
		this._mode = value;
		this.valid = value === ExtraInput.SelectMode.Free;
	}

	validate() {
		switch (this.mode) {
			case ExtraInput.SelectMode.Free:
				this.valid = true;
				break;
			case ExtraInput.SelectMode.One:
				this.valid = this.options.filter(option => option.checked).length === 1;
				break;
			case ExtraInput.SelectMode.AtLeastOne:
				this.valid = this.options.some(option => option.checked);
				break;
		}
	}
}
